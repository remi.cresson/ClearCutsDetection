/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#include "itkFixedArray.h"
#include "itkObjectFactory.h"

// Elevation handler
#include "otbWrapperElevationParametersHandler.h"
#include "otbWrapperApplicationFactory.h"

// Application engine
#include "otbStandardFilterWatcher.h"
#include "itkFixedArray.h"

// LayerStack
#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"

// Extract multichannel
#include "otbMultiChannelExtractROI.h"

// Input images list
#include "otbWrapperInputImageListParameter.h"

// Functors for NDVI operations
#include "otbDeltaNDVIFunctor.h"
#include "itkBinaryFunctorImageFilter.h"
#include "otbFunctorImageFilter.h"

// Mask handler
#include "otbMosaicFromDirectoryHandler.h"
#include "itkMaskImageFilter.h"

// Dates
#include "otbDates.h"

// Filtering
#include "itkBinaryCrossStructuringElement.h"
#include "itkBinaryBallStructuringElement.h"
//#include "itkBinaryOpeningByReconstructionImageFilter.h" // unfortunately, not streamable
#include "itkBinaryMorphologicalOpeningImageFilter.h"
#include "otbConnectedLabelsImageFilter.h"

// Vectorization
#include "otbCacheLessLabelImageToVectorData.h"

namespace otb
{

namespace Wrapper
{

class ClearCutsMultitemporalDetection : public Application
{
public:
  /** Standard class typedefs. */
  typedef ClearCutsMultitemporalDetection            Self;
  typedef Application                                Superclass;
  typedef itk::SmartPointer<Self>                    Pointer;
  typedef itk::SmartPointer<const Self>              ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(ClearCutsMultitemporalDetection, Application);

  /** Typedefs for image concatenation */
  typedef otb::ImageList<FloatImageType>                                        ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType, FloatVectorImageType>     ListConcatenerFilterType;
  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
      FloatImageType::PixelType>                                                ExtractROIFilterType;
  typedef ObjectList<ExtractROIFilterType>                                      ExtractROIFilterListType;

  /** Typedefs for temporal correlation */
  typedef unsigned char                                                         LabelValueType;
  typedef otb::VectorImage<LabelValueType>                                      LabelVectorImageType;
  typedef otb::Image<LabelValueType>                                            LabelImageType;
  typedef otb::dates::SingleDate                                                DateType;
  typedef otb::Functor::MultitemporalCorrelationFunctor<
      FloatVectorImageType::PixelType, LabelVectorImageType::PixelType>         TemporalCorrelationFunctorType;
  typedef otb::FunctorImageFilter<TemporalCorrelationFunctorType>               TemporalCorrelationFilterType;

  /** Typedefs for mask */
  typedef itk::MaskImageFilter<FloatVectorImageType, UInt8ImageType,
      FloatVectorImageType>                                                     MaskImageFilterType;
  typedef otb::MosaicFromDirectoryHandler<UInt8ImageType,
      FloatVectorImageType>                                                     MaskHandlerType;

  /** Filtering */
  typedef itk::BinaryCrossStructuringElement<LabelImageType::PixelType, 2>      CrossStructuringType;
  typedef itk::BinaryBallStructuringElement<LabelImageType::PixelType, 2>       BallStructuringType;
  typedef CrossStructuringType::Superclass                                      StructuringType;
  typedef StructuringType::RadiusType                                           RadiusType;
  typedef itk::BinaryMorphologicalOpeningImageFilter<LabelImageType,
      LabelImageType, StructuringType>                                          OpeningFilterType;
//  typedef itk::BinaryOpeningByReconstructionImageFilter<LabelImageType,
//      StructuringType>                                                          OpeningByRecFilterType;
  typedef otb::ConnectedLabelsImageFilter<LabelImageType>                       ConnectedLabelsFilterType;
  typedef MultiToMonoChannelExtractROI<LabelVectorImageType::InternalPixelType,
      LabelImageType::PixelType>                                                ExtractROILabelFilterType;
  typedef Functor::MultiThresholdFunctor<LabelImageType::PixelType,
      LabelImageType::PixelType>                                                LabelThresholdFunctorType;
  typedef itk::UnaryFunctorImageFilter<LabelImageType,LabelImageType,
      LabelThresholdFunctorType>                                                LabelThresholdFilterType;
  typedef itk::MaskImageFilter<LabelVectorImageType, LabelImageType,
      LabelVectorImageType>                                                     MaskLabelImageFilterType;

  /** Encoding */
  typedef unsigned int                                                          EncodedValueType;
  typedef otb::Image<EncodedValueType, 2>                                       EncodedImageType;
  typedef otb::Functor::EncodeVectorLabelFunctor<
      LabelVectorImageType::PixelType,EncodedImageType::PixelType>              EncodeFunctorType;
  typedef itk::UnaryFunctorImageFilter<LabelVectorImageType, EncodedImageType,
      EncodeFunctorType>                                                        EncodeFilterType;

  /** Vectorization */
  typedef otb::CacheLessLabelImageToVectorData<EncodedValueType>                VectorizeFilterType;

  /** Vector data typedefs */
  typedef VectorDataType::DataTreeType                                          DataTreeType;
  typedef itk::PreOrderTreeIterator<DataTreeType>                               TreeIteratorType;
  typedef VectorDataType::DataNodeType                                          DataNodeType;
  typedef DataNodeType::PolygonListPointerType                                  PolygonListPointerType;

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoInit()
  {

    // Documentation
    SetName("ClearCutsMultitemporalDetection");
    SetDescription("Performs clear cuts detection from NDVI time series");
    SetDocLongDescription("This application performs of clear cuts detection from time series.");
    SetDocLimitations("None");
    SetDocAuthors("Remi Cresson");

    AddDocTag(Tags::FeatureExtraction);

    // Input time series images
    AddParameter(ParameterType_InputImageList, "il", "Input time series");
    SetParameterDescription                   ("il", "The user can specify multiple NDVI input images, or one big stack of NDVI images.");

    // Input time series dates (ASCII file)
    AddParameter(ParameterType_InputFilename, "dates", "Input time series dates ASCII file (Must be YYYYMMDD format)");

    // Input (optional) masks
    AddParameter(ParameterType_Directory, "masksdir", "Vegetation masks directory");
    MandatoryOff                         ("masksdir");

    // Backward range parameters
    AddParameter(ParameterType_Group, "backward", "backward range");
    AddParameter(ParameterType_Int,   "backward.tmax", "Limit, in number of days, to look backward");
    SetMinimumParameterIntValue      ("backward.tmax", 1);
    MandatoryOff                     ("backward.tmax");
    SetDefaultParameterInt           ("backward.tmax", 2*365); // trees are there since at least 2 years
    AddParameter(ParameterType_Int,   "backward.nmin", "Minimum number of valid pixel inside backward range");
    SetMinimumParameterIntValue      ("backward.nmin", 1);
    MandatoryOff                     ("backward.nmin");
    SetDefaultParameterInt           ("backward.nmin", 5); // we want at least 5 valid pixels

    // Forward range parameters
    AddParameter(ParameterType_Group, "forward", "forward range");
    AddParameter(ParameterType_Int,   "forward.tmax", "Limit, in number of days, to look forward");
    SetMinimumParameterIntValue      ("forward.tmax", 1);
    MandatoryOff                     ("forward.tmax");
    SetDefaultParameterInt           ("forward.tmax", 2*31); // 2 months before growing again? (too much?)
    AddParameter(ParameterType_Int,   "forward.nmin", "Minimum number of valid pixel inside forward range");
    SetMinimumParameterIntValue      ("forward.nmin", 1);
    MandatoryOff                     ("forward.nmin");
    SetDefaultParameterInt           ("forward.nmin", 2); // we want at least 2 valid pixels

    // clouds flag detection
    AddParameter(ParameterType_Group, "clouds", "clouds detection");
    AddParameter(ParameterType_Float, "clouds.threshold", "Clouds drop threshold");
    SetDefaultParameterFloat         ("clouds.threshold", 0.4);
    MandatoryOff                     ("clouds.threshold");
    AddParameter(ParameterType_Int,   "clouds.tmax", "Limit, in number of days, for rejecting NDVI drop due to clouds.");
    SetDefaultParameterInt           ("clouds.tmax", 2*20); // Accorded to literature, 20 days is a good value
    MandatoryOff                     ("clouds.tmax");

    // vegetation detection
    AddParameter(ParameterType_Group, "veg", "Vegetation detection");
    AddParameter(ParameterType_Float, "veg.threshold", "Vegetation threshold");
    SetDefaultParameterFloat         ("veg.threshold", 0.4);
    MandatoryOff                     ("veg.threshold");

    // Detection range
    AddParameter(ParameterType_Group, "range",            "Detection range");
    AddParameter(ParameterType_Group, "range.start",      "Detection range start");
    AddParameter(ParameterType_Int,   "range.start.day",  "Detection range start (day)");
    SetDefaultParameterInt           ("range.start.day", 1);
    MandatoryOff                     ("range.start.day");
    AddParameter(ParameterType_Int,   "range.start.month", "Detection range start (month)");
    SetDefaultParameterInt           ("range.start.month", 5); // May
    MandatoryOff                     ("range.start.month");
    AddParameter(ParameterType_Group, "range.end",       "Detection range end");
    AddParameter(ParameterType_Int,   "range.end.day",   "Detection range end (day)");
    SetDefaultParameterInt           ("range.end.day", 31);
    MandatoryOff                     ("range.end.day");
    AddParameter(ParameterType_Int,   "range.end.month", "Detection range end (month)");
    SetDefaultParameterInt           ("range.end.month", 8); // August
    MandatoryOff                     ("range.end.month");

    // Spatial filtering
    AddParameter(ParameterType_Choice, "sfilter", "Spatial filtering method");

    AddChoice                         ("sfilter.none",         "None");

    AddChoice                         ("sfilter.connect",      "Connected components");
    AddParameter(ParameterType_Int,    "sfilter.connect.size", "Minimum number of pixels");
    SetMinimumParameterIntValue       ("sfilter.connect.size", 0 );
    SetMaximumParameterIntValue       ("sfilter.connect.size", 100);
    SetDefaultParameterInt            ("sfilter.connect.size", 5 );

    AddChoice                         ("sfilter.morpho",        "Binary morphological operation");
    AddParameter(ParameterType_Int,    "sfilter.morpho.radius", "Radius of the spatial filter");
    SetMinimumParameterIntValue       ("sfilter.morpho.radius", 0 );
    SetMaximumParameterIntValue       ("sfilter.morpho.radius", 100);
    SetDefaultParameterInt            ("sfilter.morpho.radius", 1 );
    MandatoryOff                      ("sfilter.morpho.radius");
    AddParameter(ParameterType_Choice, "sfilter.morpho.struct",       "Structuring element type");
    AddChoice                         ("sfilter.morpho.struct.ball" , "Ball");
    AddChoice                         ("sfilter.morpho.struct.cross", "Cross");
    AddParameter(ParameterType_Choice, "sfilter.morpho.op",           "Operation type");
    AddChoice                         ("sfilter.morpho.op.open",      "Opening");
//    AddChoice                         ("sfilter.morpho.op.openrec",   "Opening by reconstruction");

    // NDVI drop thresholds
    AddParameter(ParameterType_StringList, "thresholds", "NDVI drop thresholds (defaults: 0.0 0.3 0.4 1.0)");
    SetParameterDescription               ("thresholds", "The user can specify multiple NDVI drop thresholds. Use space between each value.");
    MandatoryOff                          ("thresholds");

    // Input nodata
    AddParameter(ParameterType_Float, "nodata", "Input NDVI no-data value");
    MandatoryOff                     ("nodata");
    SetDefaultParameterFloat         ("nodata", -2.0);

    // Scale the input NDVI
    AddParameter(ParameterType_Float, "inputscale", "Input scale");
    SetDefaultParameterFloat         ("inputscale", 1.0);
    MandatoryOff                     ("inputscale");

    // Output vector
    AddParameter(ParameterType_OutputVectorData, "outvec", "Output vector layer");
    SetParameterDescription                     ("outvec", "Output vector layer of detected clear cuts");
    MandatoryOff                                ("outvec");

    // Output image
    AddParameter(ParameterType_OutputImage, "outlabel", "Output label image");
    SetParameterDescription                ("outlabel", "Output label image of detected clear cuts");
    MandatoryOff                           ("outlabel");
    SetDefaultOutputPixelType              ("outlabel", ImagePixelType_uint8);

    AddRAMParameter();


    SetDocExampleParameterValue("il", "im1.tif im2.tif ... imN.tif");
    SetDocExampleParameterValue("dates", "dates.txt");
    SetDocExampleParameterValue("outlabel", "cuts.shp");
    SetDocExampleParameterValue("thresholds", "0.0 0.3 0.4 0.5 1.0");

  }

  /*
   * Retrieve the dates (numeric) from the input dates (string)
   * Input dates (string) must be formated using the following pattern: YYYYMMDD
   */
  std::vector<DateType> GetTimeSeriesDates(){

    std::vector<DateType> dates = otb::dates::GetDatesFromFile(GetParameterString("dates"));

    unsigned int n = dates.size() - 1;
    otbAppLogINFO("Using " << dates.size() << " input dates from "
        << dates[0].year << "/" << dates[0].month << "/" << dates[0].day << " to "
        << dates[n].year << "/" << dates[n].month << "/" << dates[n].day );

    return dates;
  }

  /*
   * Execute the process
   */
  void DoExecute()
  {
//    std::cout << otb::Functor::decode_value<EncodedValueType, 0 , 1> (84024325) << std::endl;
//    std::cout << otb::Functor::decode_value<EncodedValueType, 1 , 7> (84024325) << std::endl;
//    std::cout << otb::Functor::decode_value<EncodedValueType, 8 , 8> (84024325) << std::endl;
//    std::cout << otb::Functor::decode_value<EncodedValueType, 16 , 8> (84024325) << std::endl;
//return;
    // Maximum label value
    const LabelValueType maximumLabelValue = itk::NumericTraits<LabelValueType>::max();

    // Get the input image list
    FloatVectorImageListType::Pointer inList = this->GetParameterImageList("il");

    if( inList->Size() < 2 )
      {
        otbAppLogFATAL("At least two images are required.");
      }
    if ( inList->Size() >= maximumLabelValue)
      {
        otbAppLogFATAL("The maximum number of input images must be below " <<
            itk::NumericTraits<LabelValueType>::max() << ". Consider changing LabelValueType.");
      }

    // Create one stack for input NDVI images list
    m_Concatener = ListConcatenerFilterType::New();
    m_ImageList = ImageListType::New();
    m_ExtractorList = ExtractROIFilterListType::New();

    // Split each input vector image into image
    // and generate an mono channel image list
    inList->GetNthElement(0)->UpdateOutputInformation();
    FloatVectorImageType::SizeType size = inList->GetNthElement(0)->GetLargestPossibleRegion().GetSize();
    for( unsigned int i=0; i<inList->Size(); i++ )
      {
        FloatVectorImageType::Pointer vectIm = inList->GetNthElement(i);
        vectIm->UpdateOutputInformation();
        if( size != vectIm->GetLargestPossibleRegion().GetSize() )
          {
            itkExceptionMacro("Input Image size mismatch...");
          }

        for( unsigned int j=0; j<vectIm->GetNumberOfComponentsPerPixel(); j++)
          {
            ExtractROIFilterType::Pointer extractor = ExtractROIFilterType::New();
            extractor->SetInput( vectIm );
            extractor->SetChannel( j+1 );
            extractor->UpdateOutputInformation();
            m_ExtractorList->PushBack( extractor );
            m_ImageList->PushBack( extractor->GetOutput() );
          }
      }

    // Concatenate the images
    m_Concatener->SetInput( m_ImageList );
    m_Concatener->UpdateOutputInformation();

    // no-data pixel used for the stack
    unsigned int numberOfImages = m_Concatener->GetOutput()->GetNumberOfComponentsPerPixel();
    FloatVectorImageType::PixelType noDataPixel;
    noDataPixel.SetSize(numberOfImages);
    noDataPixel.Fill(GetParameterFloat("nodata"));

    // Get dates
    std::vector<DateType> dates = GetTimeSeriesDates();
    if (m_Concatener->GetOutput()->GetNumberOfComponentsPerPixel() != dates.size())
      otbAppLogFATAL("Number of images and number of dates are different! There is "
          << (m_Concatener->GetOutput()->GetNumberOfComponentsPerPixel()) << " images "
          << "and " << dates.size() << " dates.");

    // Prepare the temporal filter
    m_TemporalFilter = TemporalCorrelationFilterType::New();
    m_TemporalFilter->GetModifiableFunctor().SetInputNoDataValue(GetParameterFloat("nodata"));
    m_TemporalFilter->GetModifiableFunctor().SetMaximumNumberOfDaysBackward(GetParameterInt("backward.tmax"));
    m_TemporalFilter->GetModifiableFunctor().SetMaximumNumberOfDaysForward (GetParameterInt("forward.tmax"));
    m_TemporalFilter->GetModifiableFunctor().SetMinimumNumberOfSamplesBackward(GetParameterInt("backward.nmin"));
    m_TemporalFilter->GetModifiableFunctor().SetMinimumNumberOfSamplesForward (GetParameterInt("forward.nmin"));
    m_TemporalFilter->GetModifiableFunctor().SetDetectionRangeStartDay(GetParameterInt("range.start.day"));
    m_TemporalFilter->GetModifiableFunctor().SetDetectionRangeEndDay  (GetParameterInt("range.end.day"));
    m_TemporalFilter->GetModifiableFunctor().SetDetectionRangeStartMonth(GetParameterInt("range.start.month"));
    m_TemporalFilter->GetModifiableFunctor().SetDetectionRangeEndMonth  (GetParameterInt("range.end.month"));
    m_TemporalFilter->GetModifiableFunctor().SetDates(dates);
    m_TemporalFilter->GetModifiableFunctor().SetCloudsDropThreshold(GetParameterFloat("clouds.threshold"));
    m_TemporalFilter->GetModifiableFunctor().SetMaximumNumberOfDaysForCloudDrop(GetParameterInt("clouds.tmax"));
    m_TemporalFilter->GetModifiableFunctor().SetInputScale(GetParameterFloat("inputscale"));
    m_TemporalFilter->GetModifiableFunctor().SetVegetationThreshold(GetParameterFloat("veg.threshold"));
    if (HasValue("thresholds"))
      {
        m_TemporalFilter->GetModifiableFunctor().ClearDropThresholds();
        std::vector<std::string> threshVals = GetParameterStringList("thresholds");
        for(auto val : threshVals) {
            m_TemporalFilter->GetModifiableFunctor().PushBackDropThreshold( std::stof(val) );
        }
      }
    otbAppLogINFO("Parameters:" << m_TemporalFilter->GetModifiableFunctor().PrintSelf());

    // Mask the stack
    if (HasValue("masksdir"))
      {
        otbAppLogINFO("Using mask directory " << GetParameterAsString("masksdir"));

        // Instanciate a mask handler
        m_MaskSource = MaskHandlerType::New();
        m_MaskSource->SetDirectory(GetParameterAsString("masksdir"));
        m_MaskSource->SetReferenceImage(m_Concatener->GetOutput());
        m_MaskSource->SetUseReferenceImage(true);
        m_MaskSource->UpdateOutputInformation();

        // Mask filter
        m_MaskFilter = MaskImageFilterType::New();
        m_MaskFilter->SetInput(m_Concatener->GetOutput());
        m_MaskFilter->SetMaskImage(m_MaskSource->GetOutput());
        m_MaskFilter->SetOutsideValue(noDataPixel);
        m_MaskFilter->UpdateOutputInformation();

        // Wire to the temporal filter
        m_TemporalFilter ->SetInput(m_MaskFilter->GetOutput());
      }
    else
      {
        m_TemporalFilter->SetInput(m_Concatener->GetOutput());
      }
    m_TemporalFilter->UpdateOutputInformation();

    const LabelVectorImageType * labelVectorImage = m_TemporalFilter->GetOutput();

    if (GetParameterAsString("sfilter").compare("none") != 0)
      {

        // Extract channel 1 (ndvi drop)
        m_ExtractLabelFilter = ExtractROILabelFilterType::New();
        m_ExtractLabelFilter->SetInput(m_TemporalFilter->GetOutput());
        m_ExtractLabelFilter->SetChannel(1);

        // Create a binary image: Threshold everything which is not class 0
        m_BinaryThresholdFilter = LabelThresholdFilterType::New();
        m_BinaryThresholdFilter->SetInput(m_ExtractLabelFilter->GetOutput());
        m_BinaryThresholdFilter->GetFunctor().PushBackThreshold(0);
        m_BinaryThresholdFilter->GetFunctor().PushBackThreshold(1);
        m_BinaryThresholdFilter->GetFunctor().PushBackThreshold(maximumLabelValue);

        // Mask the vectorlabel
        m_MaskLabelImageFilter = MaskLabelImageFilterType::New();
        m_MaskLabelImageFilter->SetInput(m_TemporalFilter->GetOutput());

        // Spatial filtering
        if (GetParameterAsString("sfilter").compare("connect") == 0)
          {
            otbAppLogINFO("Using connected component based filtering");

            m_ConnectedLabelsFilter = ConnectedLabelsFilterType::New();
            m_ConnectedLabelsFilter->SetInput(m_BinaryThresholdFilter->GetOutput());
            m_ConnectedLabelsFilter->SetNoDataPixel(0);
            m_ConnectedLabelsFilter->SetMinNumberOfComponents(GetParameterInt("sfilter.connect.size"));
            m_MaskLabelImageFilter->SetMaskImage(m_ConnectedLabelsFilter->GetOutput());
          }
        else if (GetParameterAsString("sfilter").compare("morpho") == 0)
          {

            // Radius of the structuring elements
            RadiusType radius;
            radius.Fill(GetParameterInt("sfilter.morpho.radius"));

            // Structuring element
            CrossStructuringType crossStruct;
            crossStruct.SetRadius(radius);
            crossStruct.CreateStructuringElement();
            BallStructuringType ballStruct;
            ballStruct.SetRadius(radius);
            ballStruct.CreateStructuringElement();

            // Choice of the structuring element
            StructuringType se;
            if (GetParameterAsString("sfilter.morpho.struct").compare("ball") == 0)
              {
                otbAppLogINFO("Using ball structuring element");
                se = ballStruct;
              }
            else if (GetParameterAsString("sfilter.morpho.struct").compare("cross") == 0)
              {
                otbAppLogINFO("Using cross structuring element");
                se = crossStruct;
              }
            else
              {
                otbAppLogFATAL("Unknown structuring element");
              }

            if (GetParameterAsString("sfilter.morpho.op").compare("open") == 0)
              {
                otbAppLogINFO("Using morphological filtering : Opening");

                // Opening
                m_OpeningFilter = OpeningFilterType::New();
                m_OpeningFilter->SetKernel(se);
                m_OpeningFilter->SetInput(m_BinaryThresholdFilter->GetOutput());
                m_OpeningFilter->SetBackgroundValue(0);
                m_OpeningFilter->SetForegroundValue(1);
                m_MaskLabelImageFilter->SetMaskImage(m_OpeningFilter->GetOutput());
              }
            //        else if (GetParameterAsString("sfilter.morpho.op").compare("openrec") == 0)
            //          {
            //            otbAppLogINFO("Using morphological filtering : Opening by reconstruction");
            //
            //            // Opening
            //            m_OpeningByRecFilter = OpeningByRecFilterType::New();
            //            m_OpeningByRecFilter->SetKernel(se);
            //            m_OpeningByRecFilter->SetInput(m_BinaryThresholdFilter->GetOutput());
            //            m_OpeningByRecFilter->SetBackgroundValue(0);
            //            m_OpeningByRecFilter->SetForegroundValue(1);
            //            m_MaskLabelImageFilter->SetMaskImage(m_OpeningByRecFilter->GetOutput());
            //          }
            else
              {
                otbAppLogFATAL("Unknown morphological operator");
              }
          }
        else
          {
            otbAppLogFATAL("Unknown spatial filtering method");
          }

        labelVectorImage = m_MaskLabelImageFilter->GetOutput();
      }

    // Output label image
    if (HasValue("outlabel"))
      {
        SetParameterOutputImage("outlabel", m_TemporalFilter->GetOutput());
      }

    // Extract class label
    m_EncodeFilter = EncodeFilterType::New();
    m_EncodeFilter->SetInput(labelVectorImage);

    // Vectorize
    if (HasValue("outvec"))
      {
        // Execute the pipeline and create vector data
        m_VectorizeFilter = VectorizeFilterType::New();
        m_VectorizeFilter->SetInput(m_EncodeFilter->GetOutput());
        m_VectorizeFilter->SetAutomaticAdaptativeStreaming(GetParameterInt("ram"));
        AddProcess(m_VectorizeFilter, "Computing vector data");
        m_VectorizeFilter->Update();

        otbAppLogINFO("Computing vector data statistics");

        // Create a new vector data
        m_OutVectorData = VectorDataType::New();
        DataNodeType::Pointer root = m_OutVectorData->GetDataTree()->GetRoot()->Get();
        DataNodeType::Pointer document = DataNodeType::New();
        document->SetNodeType(otb::DOCUMENT);
        m_OutVectorData->GetDataTree()->Add(document, root);
        DataNodeType::Pointer folder = DataNodeType::New();
        folder->SetNodeType(otb::FOLDER);
        m_OutVectorData->GetDataTree()->Add(folder, document);
        m_OutVectorData->SetProjectionRef(m_VectorizeFilter->GetOutput()->GetProjectionRef());

        // Get input dates as string
        std::vector<std::string> datesStringList = otb::dates::GetDatesAsStringFromFile(GetParameterString("dates"));

        // Iterate on the vector data produced by the pipeline
        TreeIteratorType itVector(m_VectorizeFilter->GetOutput()->GetDataTree());
        itVector.GoToBegin();
        while (!itVector.IsAtEnd())
          {
            if (!itVector.Get()->IsRoot() && !itVector.Get()->IsDocument() && !itVector.Get()->IsFolder())
              {
                DataNodeType::Pointer currentGeometry = itVector.Get();

                EncodedValueType encodedValue = currentGeometry->GetFieldAsInt(m_VectorizeFilter->GetFieldName());

                /*
                 *     |       Attribute        | Nb bits | 1st bit |
                 *     ----------------------------------------------
                 *     | In range               |    1    |    0    |
                 *     | Class                  |    7    |    1    |
                 *     | Index of the date      |    8    |    8    |
                 *     | Nb of samples forward  |    8    |   16    |
                 *     | Nb of samples backward |    8    |   24    |
                 */

                currentGeometry->SetFieldAsInt("InRange",   otb::Functor::decode_value<EncodedValueType, 0 , 1>(encodedValue) );
                currentGeometry->SetFieldAsInt("Class",     otb::Functor::decode_value<EncodedValueType, 1 , 7>(encodedValue) );
                currentGeometry->SetFieldAsInt("NForward",  otb::Functor::decode_value<EncodedValueType, 16, 8>(encodedValue) );
                currentGeometry->SetFieldAsInt("NBackward", otb::Functor::decode_value<EncodedValueType, 24, 8>(encodedValue) );

                unsigned long dateIndex =
                    otb::Functor::decode_value<EncodedValueType, 8 , 8>(encodedValue) - 1; // Starts at 1 in the MultitemporalCorrelationFunctor
                currentGeometry->SetFieldAsString("Date",  datesStringList[dateIndex] );

                // Compute surface
                double area = currentGeometry->GetPolygonExteriorRing()->GetArea();
                double length = currentGeometry->GetPolygonExteriorRing()->GetLength();
                PolygonListPointerType holes = currentGeometry->GetPolygonInteriorRings();
                for (unsigned int hole = 0 ; hole < holes->Size() ; hole++)
                  {
                    area -= holes->GetNthElement(hole)->GetArea();
                    length -= holes->GetNthElement(hole)->GetLength();
                  }
                currentGeometry->SetFieldAsDouble("Surface", area);
                currentGeometry->SetFieldAsDouble("Length", length);

                m_OutVectorData->GetDataTree()->Add(currentGeometry, folder);
              }
            ++itVector;
          } // next feature

        otbAppLogINFO("Done.");

        SetParameterOutputVectorData("outvec", m_OutVectorData);

      } // HasValue("outvec")

} // DoExecute()

  ListConcatenerFilterType::Pointer         m_Concatener;
  ExtractROIFilterListType::Pointer         m_ExtractorList;
  ImageListType::Pointer                    m_ImageList;
  TemporalCorrelationFilterType::Pointer    m_TemporalFilter;
  MaskHandlerType::Pointer                  m_MaskSource;
  MaskImageFilterType::Pointer              m_MaskFilter;
  ExtractROILabelFilterType::Pointer        m_ExtractLabelFilter;
  OpeningFilterType::Pointer                m_OpeningFilter;
//  OpeningByRecFilterType::Pointer           m_OpeningByRecFilter;
  ConnectedLabelsFilterType::Pointer        m_ConnectedLabelsFilter;
  LabelThresholdFilterType::Pointer         m_BinaryThresholdFilter;
  MaskLabelImageFilterType::Pointer         m_MaskLabelImageFilter;
  VectorizeFilterType::Pointer              m_VectorizeFilter;
  EncodeFilterType::Pointer                 m_EncodeFilter;
  VectorDataType::Pointer                   m_OutVectorData;

}; // end of class

} // namespace wrapper
} // namespace otb

OTB_APPLICATION_EXPORT( otb::Wrapper::ClearCutsMultitemporalDetection )
