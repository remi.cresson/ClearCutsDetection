/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __MultitemporalRadiometricHarmonizationImageFilter_hxx
#define __MultitemporalRadiometricHarmonizationImageFilter_hxx

#include "otbMultitemporalRadiometricHarmonizationImageFilter.h"

#include "itkProgressReporter.h"

namespace otb
{
/**
 *
 */
template <class TImage>
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::PersitentMultitemporalRadiometricHarmonizationImageFilter()
 {
  // first output is a copy of the image, DataObject created by
  // superclass
  //
  // allocate the data objects for the outputs which are
  // just decorators around pixel types
  for (int i = 1; i < 3; ++i)
    {
    typename PixelObjectType::Pointer output
      = static_cast<PixelObjectType*>(this->MakeOutput(i).GetPointer());
    this->itk::ProcessObject::SetNthOutput(i, output.GetPointer());
    }

  // allocate the data objects for the outputs which are
  // just decorators around real matrices
  for (int i = 3; i < 6; ++i)
    {
    typename RealMatrixObjectType::Pointer output
      = static_cast<RealMatrixObjectType*>(this->MakeOutput(i).GetPointer());
    this->itk::ProcessObject::SetNthOutput(i, output.GetPointer());
    }
  typename LongMatrixObjectType::Pointer output
    = static_cast<LongMatrixObjectType*>(this->MakeOutput(6).GetPointer());
  this->itk::ProcessObject::SetNthOutput(6, output.GetPointer());


  InputImagePointer inputPtr =  const_cast<TImage *>(this->GetInput(0));
  unsigned int n = 1;
  RealMatrixType nullMatrix(n,n,itk::NumericTraits<RealType>::Zero);
  LongMatrixType longNullMatrix(n,n,itk::NumericTraits<long>::Zero);
  this->GetMeanOutput()->Set(nullMatrix);
  this->GetSigmaOutput()->Set(nullMatrix);
  this->GetMeanOfProductsOutput()->Set(nullMatrix);
  this->GetCountOutput()->Set(longNullMatrix);
 }

template <class TImage>
typename itk::DataObject::Pointer
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch (output)
  {
  case 0:
  return static_cast<itk::DataObject*>(TImage::New().GetPointer());
  break;
  case 1:
  return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
  break;
  case 2:
  return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
  break;
  case 3: // mean
  return static_cast<itk::DataObject*>(RealMatrixObjectType::New().GetPointer());
  break;
  case 4: // sqsum
  return static_cast<itk::DataObject*>(RealMatrixObjectType::New().GetPointer());
  break;
  case 5: // cosum
  return static_cast<itk::DataObject*>(RealMatrixObjectType::New().GetPointer());
  break;
  case 6: // count
  return static_cast<itk::DataObject*>(LongMatrixObjectType::New().GetPointer());
  break;
  default:
  // might as well make an image
  return static_cast<itk::DataObject*>(TImage::New().GetPointer());
  break;
  }
}


template <class TImage>
typename PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>::RealMatrixObjectType*
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::GetMeanOutput()
{
  return static_cast<RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template <class TImage>
const typename PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>::RealMatrixObjectType*
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::GetMeanOutput() const
{
  return static_cast<const RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template <class TImage>
typename PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>::RealMatrixObjectType*
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::GetSigmaOutput()
{
  return static_cast<RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(4));
}

template <class TImage>
const typename PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>::RealMatrixObjectType*
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::GetSigmaOutput() const
{
  return static_cast<const RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(4));
}

template <class TImage>
typename PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>::RealMatrixObjectType*
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::GetMeanOfProductsOutput()
{
  return static_cast<RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(5));
}

template <class TImage>
const typename PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>::RealMatrixObjectType*
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::GetMeanOfProductsOutput() const
{
  return static_cast<const RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(5));
}

template <class TImage>
typename PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>::LongMatrixObjectType*
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::GetCountOutput()
{
  return static_cast<LongMatrixObjectType*>(this->itk::ProcessObject::GetOutput(6));
}

template <class TImage>
const typename PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>::LongMatrixObjectType*
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::GetCountOutput() const
{
  return static_cast<const LongMatrixObjectType*>(this->itk::ProcessObject::GetOutput(6));
}

template <class TImage>
void
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
    this->GetOutput()->CopyInformation(this->GetInput());
    this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());

    if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
      {
      this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
      }
    }
}

template <class TImage>
void
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}


/*
 * Synthetize internal counters
 */
template <class TImage>
void
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::Synthetize()
{

  // Prepare container for final results
  InputImagePointer inputPtr =  const_cast<TImage *>(this->GetInput(0));
  unsigned int n = inputPtr->GetNumberOfComponentsPerPixel();
  m_FinalStatsContainer = ThreadResultsContainer(n);

  // Gather threads containers
  int numberOfThreads = this->GetNumberOfThreads();
  for (unsigned int i = 0; i < numberOfThreads; ++i)
    {
    m_FinalStatsContainer.Update(m_StatsContainers[i]);
    }

  // Create final stats matrices
  RealMatrixType means(n,n,itk::NumericTraits<RealType>::Zero);
  RealMatrixType stds(n,n,itk::NumericTraits<RealType>::Zero);
  RealMatrixType meansofproducts(n,n,itk::NumericTraits<RealType>::Zero);
  LongMatrixType counts(n,n,itk::NumericTraits<long>::Zero);

  // Update container for final results
  for (unsigned int i = 0 ; i < n ; i++)
    {
    for (unsigned int j = 0 ; j < n ; j++)
      {
      long count = m_FinalStatsContainer.m_count[i][j];
      RealType sum = m_FinalStatsContainer.m_sum[i][j];
      RealType sumOfSquares = m_FinalStatsContainer.m_sqSum[i][j];
      RealType sumOfProducts = m_FinalStatsContainer.m_coSum[i][j];

      RealType mean = itk::NumericTraits<RealType>::Zero;
      RealType sigma = itk::NumericTraits<RealType>::Zero;
      RealType meansofproduct = itk::NumericTraits<RealType>::Zero;

      if (count > 0)
        {
        // compute statistics
        mean = sum / static_cast<RealType>(count);
        meansofproduct = sumOfProducts / static_cast<RealType>(count);
        if (count > 1)
          {
          // unbiased estimate
          RealType variance = (sumOfSquares - (sum * sum / static_cast<RealType>(count)))
                         / static_cast<RealType>(count - 1);
          sigma = vcl_sqrt(variance);
          }
        }

      means[i][j] = mean;
      stds[i][j] = sigma;
      meansofproducts[i][j] = meansofproduct;
      counts[i][j] = count;

      } // next j
    } // next i


  // Set the outputs
  this->GetMeanOutput()->Set(means);
  this->GetSigmaOutput()->Set(stds);
  this->GetMeanOfProductsOutput()->Set(meansofproducts);
  this->GetCountOutput()->Set(counts);

}

/*
 * Reset internal counters
 */
template <class TImage>
void
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::Reset()
{
  unsigned int numberOfThreads = this->GetNumberOfThreads();
  InputImagePointer inputPtr =  const_cast<TImage *>(this->GetInput(0));
  if (!inputPtr)
    {
    itkExceptionMacro("No input specified");
    }
  unsigned int n = inputPtr->GetNumberOfComponentsPerPixel();

  m_StatsContainers.clear();
  for (unsigned int i = 0; i < numberOfThreads; ++i)
    {
    ThreadResultsContainer emptyContainer = ThreadResultsContainer(n);
    m_StatsContainers.push_back(emptyContainer);
    }

}

/**
 *
 */
template <class TImage>
void
PersitentMultitemporalRadiometricHarmonizationImageFilter<TImage>
::ThreadedGenerateData(const ImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
 {

  // Debug info
  itkDebugMacro(<<"Actually executing thread " << threadId << " in region " << outputRegionForThread);

  // Support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels() );

  // Iterate through the thread region
  InputImageIteratorType inputIt(this->GetInput(), outputRegionForThread);

  // Compute stats
  for ( inputIt.GoToBegin(); !inputIt.IsAtEnd(); ++inputIt )
    {
    ImagePixelType stack = inputIt.Get();
    for (unsigned int i = 0; i < stack.GetSize(); i++)
      {
      RealType value_t1 = stack[i];
      if (value_t1 != m_NoDataValue)
        {
        for (unsigned int j = 0 ; j < stack.GetSize(); j++)
          {
          RealType value_t0 = stack[j];
          if (value_t0 != m_NoDataValue)
            {
            m_StatsContainers[threadId].Update(value_t0, value_t1, i, j);
            }
          }
        }
      }

    } // Next pixel
 }
}
#endif



