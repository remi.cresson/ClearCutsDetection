/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __otbDeltaNDVIFunctor_h
#define __otbDeltaNDVIFunctor_h

#include "boost/date_time/gregorian/gregorian.hpp"
#include "itkNumericTraits.h"
#include "vnl/vnl_vector.h"
#include "vnl/vnl_matrix.h"
#include <vector>
#include <bitset>
#include "otbDates.h"

namespace otb
{

namespace Functor
{

/** \class DeltaNDVI
 *  \brief This functor computes the difference between NDVI from two pixels
 *
 *  \ingroup Functor
 *
 */
template< class TPixel, class TOutputValue>
class DeltaNDVIFromChannels
{
public:
  DeltaNDVIFromChannels() {noData=3.0; nirChannelT0=0; nirChannelT1=0; redChannelT0=0; redChannelT1=0;}
  ~DeltaNDVIFromChannels() {}
  bool operator!=(const DeltaNDVIFromChannels &) const{
    return false;
  }

  bool operator==(const DeltaNDVIFromChannels & other) const{
    return !( *this != other );
  }

  inline TOutputValue operator()(const TPixel & T0,
      const TPixel & T1) const
  {
    double nir_t0 = static_cast< double >( T0[nirChannelT0]);
    double nir_t1 = static_cast< double >( T1[nirChannelT1]);
    double red_t0 = static_cast< double >( T0[redChannelT0]);
    double red_t1 = static_cast< double >( T1[redChannelT1]);
    if (vcl_abs(nir_t0 + red_t0) > 0.0001 && vcl_abs(nir_t1 + red_t1) > 0.0001)
      {
        return static_cast< TOutputValue >(
            (nir_t1 - red_t1) / (nir_t1 + red_t1)
            -(nir_t0 - red_t0) / (nir_t0 + red_t0) );
      }
    else
      {
        return static_cast< TOutputValue >( noData );
      }
  }

  void SetNoDataValue(double value) {noData = value; }
  void SetNIRChannelT0(unsigned int number) {nirChannelT0 = number - 1;}
  void SetRedChannelT0(unsigned int number) {redChannelT0 = number - 1;}
  unsigned int GetNIRChannelT0() {return nirChannelT0 ;}
  unsigned int GetRedChannelT0() {return redChannelT0 ;}

  double GetNoDataValue() {return noData;}
  void SetNIRChannelT1(unsigned int number) {nirChannelT1 = number - 1;}
  void SetRedChannelT1(unsigned int number) {redChannelT1 = number - 1;}
  unsigned int GetNIRChannelT1() {return nirChannelT1 ;}
  unsigned int GetRedChannelT1() {return redChannelT1 ;}


private:
  unsigned int nirChannelT0;
  unsigned int redChannelT0;
  unsigned int nirChannelT1;
  unsigned int redChannelT1;
  double noData;

};

/** \class MultitemporalCorrelationFunctor
 *  \brief This functor computes the "NDVI drop" index
 *
 *  \ingroup Functor
 */
template <class TNDVIStack, class TOutputLabel>
class MultitemporalCorrelationFunctor
{
  typedef typename TNDVIStack::ValueType NDVIValueType;
  typedef typename TOutputLabel::ValueType OutputLabelValueType;
  typedef otb::dates::SingleDate DateType;
  typedef std::vector<DateType> DatesType;

public:

  /** Return the index name */
  virtual std::string GetName() const
  {
    return "Multitemporal NDVI correlation functor";
  }

  std::size_t OutputSize(const std::array<size_t, 1>&) const
  {
    return outputPixelSize;
  }

  std::size_t OutputSize() const
  {
    return outputPixelSize;
  }

  // Constructor
  MultitemporalCorrelationFunctor() {

    /*
     * Pixel components:
     * 1: ndvi drop (class)
     * 2: ndvi drop (date)
     * 3: nb of images backward
     * 4: nb of images forward
     * 5: is the detection inside the forward range?
     * 6: total number of valid values
     * 7: date of the image available before the ndvi drop
     */

    // input no-data value
    m_InputNoDataValue = -2.0;

    // Output no-data pixel value
    m_OutputNoDataPixel.SetSize(outputPixelSize);
    m_OutputNoDataPixel.Fill(itk::NumericTraits<OutputLabelValueType>::Zero);

    // Forward/Backward parameters
    m_MaximumNumberOfDaysBackward    = 31;  // one month
    m_MaximumNumberOfDaysForward     = 31;  // one month
    m_MinimumNumberOfSamplesBackward = 1;   // one sample
    m_MinimumNumberOfSamplesForward  = 1;   // one sample

    // detection range
    m_DetectionRangeStartDay   = 1;
    m_DetectionRangeStartMonth = 5; // may
    m_DetectionRangeEndDay     = 31;
    m_DetectionRangeEndMonth   = 8; // august

    // Cloud detection
    m_CloudsDropThreshold = 0.4;
    m_MaximumNumberOfDaysForCloudDrop = 20;

    // Vegetation detection
    m_VegetationThreshold = 0.4;

    // Thresholds
    m_Thresholds.push_back(0.0);
    m_Thresholds.push_back(0.3);
    m_Thresholds.push_back(0.4);
    m_Thresholds.push_back(1.0);

    // Input scale
    m_InputScale = 1.0;
  }

  // Destructor
  virtual ~MultitemporalCorrelationFunctor(){}

  // No data set/get
  void SetInputNoDataValue(NDVIValueType value) { m_InputNoDataValue = value; }
  NDVIValueType GetInputNoDataValue() { return m_InputNoDataValue; }
  void SetOutputNoDataPixel(TOutputLabel pixel) { m_OutputNoDataPixel = pixel; }
  TOutputLabel GetOutputNoDataPixel() { return m_OutputNoDataPixel; }

  /*
   * Print self
   */
  std::string PrintSelf()
  {
    std::string out;
    out += "\nm_MaximumNumberOfDaysBackward: " + std::to_string(m_MaximumNumberOfDaysBackward);
    out += "\nm_MaximumNumberOfDaysForward: " + std::to_string(m_MaximumNumberOfDaysForward);
    out += "\nm_MinimumNumberOfSamplesBackward: " + std::to_string(m_MinimumNumberOfSamplesBackward);
    out += "\nm_MinimumNumberOfSamplesForward: " + std::to_string(m_MinimumNumberOfSamplesForward);
    out += "\nm_DetectionRangeStartDay: " + std::to_string(m_DetectionRangeStartDay);
    out += "\nm_DetectionRangeStartMonth: " + std::to_string(m_DetectionRangeStartMonth);
    out += "\nm_MaximumNumberOfDaysForCloudDrop: " + std::to_string(m_MaximumNumberOfDaysForCloudDrop);
    out += "\nm_CloudsDropThreshold: " + std::to_string(m_CloudsDropThreshold);
    out += "\nm_InputScale: " + std::to_string(m_InputScale);
    out += "\nm_VegetationThreshold: " + std::to_string(m_VegetationThreshold);
    out += "\nThresholds (class):";
    for (unsigned int i = 0 ; i < m_Thresholds.size() ; i++)
      {
        out += "\n" + std::to_string(m_Thresholds[i]) + "(" + std::to_string(i) + ")";
      }
    return out;
  }

  /*
   * Compute the output pixel
   */
  inline TOutputLabel operator()(const TNDVIStack & pixel) const
  {

    // Output pixel
    TOutputLabel outLabel (m_OutputNoDataPixel);

    int n = pixel.GetSize();
    const OutputLabelValueType noDateSelected (itk::NumericTraits<OutputLabelValueType>::Zero);
    NDVIValueType maxDrop(itk::NumericTraits<NDVIValueType>::NonpositiveMin());
    OutputLabelValueType dateOfMaxDrop(noDateSelected), previousDateOfMaxDrop(noDateSelected);
    bool maxDropIsInsideForwardRange = false;
    int fwnvals=0;
    int bwnvals=0;

    TNDVIStack maskedSeries(pixel);

    // Rescale the input if needed
    if (m_InputScale != 1.0)
      {
        for (int i = 0 ; i < n; i++)
          {
            maskedSeries[i] *= m_InputScale;
          }
      }

    // Reject clouds
    // Set no data value where there is an increase of at least
    // m_Treshold during a time window shorted than m_ThresholdDuration
    for (int i = 0 ; i < n-1 ; i++)
      {
        if (maskedSeries[i] != m_InputNoDataValue)
          {
            // Look in the future and check there is no odd increase
            for (int j = i + 1; j < n; j++)
              {
                if (m_Dates[j].julianday - m_Dates[i].julianday <= m_MaximumNumberOfDaysForCloudDrop)
                  {
                    if ( (maskedSeries[j] != m_InputNoDataValue) && (maskedSeries[j] - maskedSeries[i] > m_CloudsDropThreshold))
                      {
                        // We mark this sample as invalid and process the next one
                        maskedSeries[i] = m_InputNoDataValue;
                        break;
                      }
                  }
                else
                  {
                    // Exit the loop since we are out of time window
                    break;
                  }
              }
          } // current sample is different than no data
      }

    // Comput nb. of valid pixels
    int nbValidPixels = 0;
    for (int i = 0 ; i < n; i++)
      {
        if (maskedSeries[i] != m_InputNoDataValue)
          nbValidPixels++;
      }
    outLabel[5] = static_cast<OutputLabelValueType>(nbValidPixels);

    // Compute drop at sample i
    for (int i = 0 ; i < n; i++)
      {
        if (m_Dates[i].IsInRange(m_DetectionRangeStartMonth,m_DetectionRangeStartDay, m_DetectionRangeEndMonth, m_DetectionRangeEndDay))
          {
            if (maskedSeries[i] != m_InputNoDataValue)
              {
                bool lastSampleIsInsideForwardRange = false;

                // Julian day
                int currentJulianDay = m_Dates[i].julianday;

                // Compute forward value from the sample after the i-th, included this one
                NDVIValueType forwardRangeValue = 0;
                int forwardRangeNumberOfSamples = 0;
                int increment = 0;
                while (true)
                  {
                    // Index of sample to sum
                    int index = i + increment;
                    if (index < n)
                      {
                        if (maskedSeries[index] != m_InputNoDataValue)
                          {
                            bool insideForwardRange = ((m_Dates[index].julianday - currentJulianDay) <= m_MaximumNumberOfDaysForward);
                            bool enoughSamples = (forwardRangeNumberOfSamples < m_MinimumNumberOfSamplesForward);
                            if (insideForwardRange || enoughSamples)
                              {
                                forwardRangeNumberOfSamples++;
                                forwardRangeValue += maskedSeries[index];
                                lastSampleIsInsideForwardRange = insideForwardRange;
                              }
                            else
                              {
                                // Outside the forward range, and enough samples
                                break;
                              }
                          }
                      }
                    else
                      {
                        // End of signal reached
                        break;
                      }
                    increment++;
                  }
                if (forwardRangeNumberOfSamples >= m_MinimumNumberOfSamplesForward)
                  {

                    // Compute backward value from the sample just before the i-th
                    NDVIValueType backwardRangeValue = 0;
                    int backwardRangeNumberOfSamples = 0;
                    increment = 0;
                    while (true)
                      {
                        increment--;
                        // Index of sample to sum
                        int index = i + increment;
                        if (index >= 0)
                          {
                            if (maskedSeries[index] != m_InputNoDataValue)
                              {
                                if ((currentJulianDay - m_Dates[index].julianday) <= m_MaximumNumberOfDaysBackward)
                                  {
                                	if (previousDateOfMaxDrop == noDateSelected)
                                		previousDateOfMaxDrop = index;
                                    backwardRangeNumberOfSamples++;
                                    backwardRangeValue += maskedSeries[index];
                                  }
                                else
                                  {
                                    // Outside the backward range
                                    break;
                                  }
                              }
                          }
                        else
                          {
                            // Beginning of signal reached
                            break;
                          }
                      }
                    if (backwardRangeNumberOfSamples >= m_MinimumNumberOfSamplesBackward)
                      {
                        forwardRangeValue  /= static_cast<NDVIValueType>(forwardRangeNumberOfSamples);
                        backwardRangeValue /= static_cast<NDVIValueType>(backwardRangeNumberOfSamples);

                        if (backwardRangeValue > m_VegetationThreshold)
                          {
                            // Update the attributes at maximum drop
                            NDVIValueType drop = backwardRangeValue - forwardRangeValue;
                            if (maxDrop < drop)
                              {
                                maxDrop = drop;
                                dateOfMaxDrop = i + 1;
                                fwnvals = forwardRangeNumberOfSamples;
                                bwnvals = backwardRangeNumberOfSamples;
                                maxDropIsInsideForwardRange = lastSampleIsInsideForwardRange;
                              }
                          } // the backward range targets vegetation
                      } // enough samples in backward range
                  } // enough samples in forward range
              } // current pixel different than no-data
          } // current date is inside detection range
      } // process next sample

    if (dateOfMaxDrop != noDateSelected)
      {

        // Clamp maxDrop value in [0,1]
        float v = maxDrop;
        if (v < 0.0) v = 0.0;
        if (v > 1.0) v = 1.0;

        // Assign output pixel for classes > 0
        for (unsigned int i = 1 ; i < m_Thresholds.size()-1 ; i++)
          {
            NDVIValueType lowThresh = m_Thresholds[i];
            NDVIValueType hiThresh = m_Thresholds[i+1];
            if (lowThresh <= v && v < hiThresh)
              {
                outLabel[0] = static_cast<OutputLabelValueType>(i);
                outLabel[1] = static_cast<OutputLabelValueType>(dateOfMaxDrop);
                outLabel[2] = static_cast<OutputLabelValueType>(fwnvals);
                outLabel[3] = static_cast<OutputLabelValueType>(bwnvals);
                outLabel[4] = static_cast<OutputLabelValueType>(maxDropIsInsideForwardRange);
                outLabel[5] = static_cast<OutputLabelValueType>(nbValidPixels);
                outLabel[6] = static_cast<OutputLabelValueType>(previousDateOfMaxDrop);
                break;
              }
          }
//        outLabel[0] = static_cast<OutputLabelValueType>(v*100);
      }

    return outLabel;
  }

  void SetMaximumNumberOfDaysBackward(int n)     {m_MaximumNumberOfDaysBackward = n;     }
  void SetMaximumNumberOfDaysForward(int n)      {m_MaximumNumberOfDaysForward = n;      }
  void SetMinimumNumberOfSamplesBackward(int n)  {m_MinimumNumberOfSamplesBackward = n;  }
  void SetMinimumNumberOfSamplesForward(int n)   {m_MinimumNumberOfSamplesForward = n;   }
  void SetDetectionRangeStartDay(int n)          {m_DetectionRangeStartDay = n;          }
  void SetDetectionRangeStartMonth(int n)        {m_DetectionRangeStartMonth = n;        }
  void SetDetectionRangeEndDay(int n)            {m_DetectionRangeEndDay = n;            }
  void SetDetectionRangeEndMonth(int n)          {m_DetectionRangeEndMonth = n;          }
  void SetDates(DatesType dates)                 {m_Dates = dates;                       }
  void SetMaximumNumberOfDaysForCloudDrop(int n) {m_MaximumNumberOfDaysForCloudDrop = n; }
  void SetCloudsDropThreshold(float value)       {m_CloudsDropThreshold = value;         }
  void SetInputScale(float value)                {m_InputScale = value;                  }
  void SetVegetationThreshold(float value)       {m_VegetationThreshold = value;         }

  // Set thresholds
  void PushBackDropThreshold(float value)
  {
    m_Thresholds.push_back(value);
    std::sort (m_Thresholds.begin(), m_Thresholds.end());
  }
  void ClearDropThresholds()
  {
    m_Thresholds.clear();
  }

private:
  // no data values
  NDVIValueType m_InputNoDataValue;
  TOutputLabel m_OutputNoDataPixel;

  // Backward/Forward ranges parameters
  int m_MaximumNumberOfDaysBackward;
  int m_MaximumNumberOfDaysForward;
  int m_MinimumNumberOfSamplesBackward;
  int m_MinimumNumberOfSamplesForward;

  // detection range
  int m_DetectionRangeStartDay;
  int m_DetectionRangeStartMonth;
  int m_DetectionRangeEndDay;
  int m_DetectionRangeEndMonth;

  // images dates
  DatesType m_Dates;

  // Cloud detection
  float m_CloudsDropThreshold;
  int   m_MaximumNumberOfDaysForCloudDrop;

  // Vegetation detection
  float m_VegetationThreshold;

  // Use multiplicator at input
  float m_InputScale;

  std::vector<NDVIValueType> m_Thresholds;
  static constexpr std::size_t outputPixelSize{7};

};


/*
 * \class MultiThresholdFunctor
 * \brief Perform a multiple thresholding on the input value.
 * Output is a label pixel with integer values.
 *
 * \ingroup ClearCutsDetection
 */
template< class TPixel, class TLabel>
class MultiThresholdFunctor
{
public:
  MultiThresholdFunctor() { m_OutputFirstClassLabel = 0;}
  ~MultiThresholdFunctor() {}
  bool operator!=(const MultiThresholdFunctor &) const{
    return false;
  }

  bool operator==(const MultiThresholdFunctor & other) const{
    return !( *this != other );
  }

  void PushBackThreshold(TPixel value)
  {
    m_Thresholds.push_back(value);
  }

  inline TLabel operator()(const TPixel & pixel) const
  {
    TLabel out(m_OutputFirstClassLabel);
    for (unsigned int i = 0 ; i < m_Thresholds.size()-1 ; i++)
      {
        TPixel lowThresh = m_Thresholds[i];
        TPixel hiThresh = m_Thresholds[i+1];
        if (lowThresh <= pixel && pixel < hiThresh)
          {
            out = static_cast<TLabel>(m_OutputFirstClassLabel + i);
          }
      }
    return out;
  }

private:
  std::vector<TPixel> m_Thresholds;
  TLabel m_OutputFirstClassLabel;
}; // MultiThresholdFunctor

/*
 * \class EncodeVectorLabelFunctor
 * \brief Encode the input vector label into an UInt32 output.
 *
 * This functor encodes the output pixel of MultitemporalCorrelationFunctor into 32 bits:
 * -isInRange flag      (1 bit)
 * -Class               (7 bits)
 * -Date                (8 bits)
 * -N samples forward   (8 bits)
 * -N samples backward  (8 bits)
 *
 * \ingroup ClearCutsDetection
 */
template< class TVectorLabel, class TLabel>
class EncodeVectorLabelFunctor
{
public:
  EncodeVectorLabelFunctor() {}
  ~EncodeVectorLabelFunctor() {}
  bool operator!=(const EncodeVectorLabelFunctor &) const{ return false; }
  bool operator==(const EncodeVectorLabelFunctor & other) const{ return !( *this != other ); }

  inline TLabel operator()(const TVectorLabel & label) const
  {
    TLabel out = 0;

    out += 1 * label[4]; // 1 bit  starting from bit 0 (1=2^0)
    out += 2 * label[0]; // 7 bits starting from bit 1 (2=2^1)
    out += 256 * label[1]; // 8 bits starting from bit 8 (256=2^8)
    out += 65536 * label[2]; // 8 bits starting from bit 16 (65536=2^16)
    out += 16777216 * label[3]; // 8 bits starting from bit 24 (16777216=2^24)

    return out;
  }


}; // EncodeVectorLabelFunctor


/*
 * Decode the value
 */
template<class TValueType, std::size_t START, std::size_t LENGHT>
unsigned long decode_value(TValueType encodedValue)
{
  const std::size_t NBITS = std::numeric_limits<TValueType>::digits;
  std::bitset<NBITS> bits = std::bitset<NBITS>(encodedValue);
  bits <<= (NBITS - LENGHT - START);
  bits >>= (NBITS - LENGHT);

  return bits.to_ulong();
}

} // namespace Functor
} // namespace otb

#endif

