/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __MultitemporalStatisticsImageFilter_hxx
#define __MultitemporalStatisticsImageFilter_hxx

#include "otbMultitemporalStatisticsImageFilter.h"

#include "itkProgressReporter.h"

namespace otb
{
/**
 *
 */
template <class TImage, class TFunctor>
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::PersitentMultitemporalStatisticsImageFilter()
 {
  // first output is a copy of the image, DataObject created by
  // superclass
  //
  // allocate the data objects for the outputs which are
  // just decorators around pixel types
  for (int i = 1; i < 3; ++i)
    {
    typename PixelObjectType::Pointer output
      = static_cast<PixelObjectType*>(this->MakeOutput(i).GetPointer());
    this->itk::ProcessObject::SetNthOutput(i, output.GetPointer());
    }

  // allocate the data objects for the outputs which are
  // just decorators around real matrices
  for (int i = 3; i < 7; ++i)
    {
    typename RealMatrixObjectType::Pointer output
      = static_cast<RealMatrixObjectType*>(this->MakeOutput(i).GetPointer());
    this->itk::ProcessObject::SetNthOutput(i, output.GetPointer());
    }

  InputImagePointer inputPtr =  const_cast<TImage *>(this->GetInput(0));
  unsigned int n = 1;
  RealMatrixType nullMatrix(n,n,itk::NumericTraits<RealType>::Zero);
  this->GetMeanOutput()->Set(nullMatrix);
  this->GetSigmaOutput()->Set(nullMatrix);

 }

template <class TImage, class TFunctor>
typename itk::DataObject::Pointer
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::MakeOutput(DataObjectPointerArraySizeType output)
{
  switch (output)
    {
    case 0:
      return static_cast<itk::DataObject*>(TImage::New().GetPointer());
      break;
    case 1:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 2:
      return static_cast<itk::DataObject*>(PixelObjectType::New().GetPointer());
      break;
    case 3:
    return static_cast<itk::DataObject*>(RealMatrixObjectType::New().GetPointer());
    break;
    case 4:
    return static_cast<itk::DataObject*>(RealMatrixObjectType::New().GetPointer());
    break;

    case 5:
    case 6:
      return static_cast<itk::DataObject*>(RealObjectType::New().GetPointer());
      break;
    default:
      // might as well make an image
      return static_cast<itk::DataObject*>(TImage::New().GetPointer());
      break;
    }
}


template <class TImage, class TFunctor>
typename PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>::RealMatrixObjectType*
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::GetMeanOutput()
{
  return static_cast<RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template <class TImage, class TFunctor>
const typename PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>::RealMatrixObjectType*
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::GetMeanOutput() const
{
  return static_cast<const RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(3));
}

template <class TImage, class TFunctor>
typename PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>::RealMatrixObjectType*
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::GetSigmaOutput()
{
  return static_cast<RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(4));
}

template <class TImage, class TFunctor>
const typename PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>::RealMatrixObjectType*
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::GetSigmaOutput() const
{
  return static_cast<const RealMatrixObjectType*>(this->itk::ProcessObject::GetOutput(4));
}

template <class TImage, class TFunctor>
void
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::GenerateOutputInformation()
{
  Superclass::GenerateOutputInformation();
  if (this->GetInput())
    {
    this->GetOutput()->CopyInformation(this->GetInput());
    this->GetOutput()->SetLargestPossibleRegion(this->GetInput()->GetLargestPossibleRegion());

    if (this->GetOutput()->GetRequestedRegion().GetNumberOfPixels() == 0)
      {
      this->GetOutput()->SetRequestedRegion(this->GetOutput()->GetLargestPossibleRegion());
      }
    }
}

template <class TImage, class TFunctor>
void
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::AllocateOutputs()
{
  // This is commented to prevent the streaming of the whole image for the first stream strip
  // It shall not cause any problem because the output image of this filter is not intended to be used.
  //InputImagePointer image = const_cast< TInputImage * >( this->GetInput() );
  //this->GraftOutput( image );
  // Nothing that needs to be allocated for the remaining outputs
}


/*
 * Synthetize internal counters
 */
template <class TImage, class TFunctor>
void
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::Synthetize()
{

  // Prepare container for final results
  InputImagePointer inputPtr =  const_cast<TImage *>(this->GetInput(0));
  unsigned int n = inputPtr->GetNumberOfComponentsPerPixel();
  m_FinalStatsContainer = ThreadResultsContainer(n);

  // Gather threads containers
  int numberOfThreads = this->GetNumberOfThreads();
  for (unsigned int i = 0; i < numberOfThreads; ++i)
    {
    m_FinalStatsContainer.Update(m_StatsContainers[i]);
    }

  // Create final stats matrices
  RealMatrixType means(n,n,itk::NumericTraits<RealType>::Zero);
  RealMatrixType stds(n,n,itk::NumericTraits<RealType>::Zero);

  // Update container for final results
  for (unsigned int i = 0 ; i < n ; i++)
    {
    for (unsigned int j = 0 ; j < i ; j++)
      {
      long count = m_FinalStatsContainer.m_count[i][j];
      RealType sum = m_FinalStatsContainer.m_sum[i][j];
      RealType sumOfSquares = m_FinalStatsContainer.m_sqSum[i][j];

      RealType mean = itk::NumericTraits<RealType>::Zero;
      RealType sigma = itk::NumericTraits<RealType>::Zero;

      if (count > 0)
        {
        // compute statistics
        mean = sum / static_cast<RealType>(count);
        if (count > 1)
          {
          // unbiased estimate
          RealType variance = (sumOfSquares - (sum * sum / static_cast<RealType>(count)))
                         / static_cast<RealType>(count - 1);
          sigma = vcl_sqrt(variance);
          }
        }

      means[i][j] = mean;
      stds[i][j] = sigma;

      } // next j
    } // next i


  // Set the outputs
  this->GetMeanOutput()->Set(means);
  this->GetSigmaOutput()->Set(stds);
}

/*
 * Reset internal counters
 */
template <class TImage, class TFunctor>
void
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::Reset()
{
  unsigned int numberOfThreads = this->GetNumberOfThreads();
  InputImagePointer inputPtr =  const_cast<TImage *>(this->GetInput(0));
  if (!inputPtr)
    {
    itkExceptionMacro("No input specified");
    }
  unsigned int n = inputPtr->GetNumberOfComponentsPerPixel();

  m_StatsContainers.clear();
  for (unsigned int i = 0; i < numberOfThreads; ++i)
    {
    ThreadResultsContainer emptyContainer = ThreadResultsContainer(n);
    m_StatsContainers.push_back(emptyContainer);
    }

}

/**
 *
 */
template <class TImage, class TFunctor>
void
PersitentMultitemporalStatisticsImageFilter<TImage, TFunctor>
::ThreadedGenerateData(const ImageRegionType& outputRegionForThread, itk::ThreadIdType threadId)
 {

  // Debug info
  itkDebugMacro(<<"Actually executing thread " << threadId << " in region " << outputRegionForThread);

  // Support progress methods/callbacks
  itk::ProgressReporter progress(this, threadId, outputRegionForThread.GetNumberOfPixels() );

  // Iterate through the thread region
  InputImageIteratorType inputIt(this->GetInput(), outputRegionForThread);

  // Compute stats
  TFunctor functor;
  for ( inputIt.GoToBegin(); !inputIt.IsAtEnd(); ++inputIt )
    {
    ImagePixelType stack = inputIt.Get();
    for (unsigned int i = 1; i < stack.GetSize(); i++) // Start from the second component (t1 must be > t0)
      {
      RealType value_t1 = stack[i];
      if (value_t1 != m_NoDataValue)
        {
        for (unsigned int j = 0 ; j < i; j++) // Start from the first component
          {
          RealType value_t0 = stack[j];
          if (value_t0 != m_NoDataValue)
            {
            RealType result = functor(value_t0, value_t1);
            m_StatsContainers[threadId].Update(result, i, j);
            }
          }
        }
      }

    } // Next pixel
 }
}
#endif



