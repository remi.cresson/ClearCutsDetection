/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef MultitemporalStatisticsImageFilter_H_
#define MultitemporalStatisticsImageFilter_H_

#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "otbPersistentFilterStreamingDecorator.h"

// No data
#include "otbNoDataHelper.h"

// Matrix
#include "vnl/vnl_matrix.h"

namespace otb
{

/**
 * \class MultitemporalStatisticsImageFilter
 * \brief Compute stats related to the binary functor, for each couple
 * of input image
 *
 * Output: mean, std
 *
 * \ingroup ClearCutsDetection
 */
template <class TImage, class TFunctor>
class ITK_EXPORT PersitentMultitemporalStatisticsImageFilter :
public PersistentImageFilter<TImage, TImage>
{

public:

  /** Standard class typedefs. */
  typedef PersitentMultitemporalStatisticsImageFilter   Self;
  typedef PersistentImageFilter<TImage, TImage>	Superclass;
  typedef itk::SmartPointer<Self>                 Pointer;
  typedef itk::SmartPointer<const Self>           ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(PersitentMultitemporalStatisticsImageFilter, PersistentImageFilter);

  /** Iterators typedefs */
  typedef TImage ImageType;

  typedef typename ImageType::Pointer       InputImagePointer;
  typedef typename ImageType::PixelType     ImagePixelType;
  typedef typename ImageType::InternalPixelType   RealType;
  typedef typename ImageType::IndexType     ImageIndexType;
  typedef typename ImageType::RegionType    ImageRegionType;
  typedef typename itk::ImageRegionConstIterator<ImageType>   InputImageIteratorType;
  typedef vnl_matrix<RealType>     RealMatrixType;
  typedef vnl_matrix<long>         LongMatrixType;

  itkStaticConstMacro(InputImageDimension, unsigned int,
      TImage::ImageDimension);

  /** Image related typedefs. */
  itkStaticConstMacro(ImageDimension, unsigned int,
      TImage::ImageDimension);

  /** Smart Pointer type to a DataObject. */
  typedef typename itk::DataObject::Pointer DataObjectPointer;
  typedef itk::ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<ImagePixelType> PixelObjectType;
  typedef itk::SimpleDataObjectDecorator<RealMatrixType>  RealMatrixObjectType;
  typedef itk::SimpleDataObjectDecorator<LongMatrixType> LongMatrixObjectType;

  /** Return the computed Mean. */
  RealMatrixType GetMean() const
  {
    return this->GetMeanOutput()->Get();
  }
  RealMatrixObjectType* GetMeanOutput();
  const RealMatrixObjectType* GetMeanOutput() const;

  /** Return the computed Standard Deviation. */
  RealMatrixType GetSigma() const
  {
    return this->GetSigmaOutput()->Get();
  }
  RealMatrixObjectType* GetSigmaOutput();
  const RealMatrixObjectType* GetSigmaOutput() const;

  /** Make a DataObject of the correct type to be used as the specified
   * output. */
  DataObjectPointer MakeOutput(DataObjectPointerArraySizeType idx) ITK_OVERRIDE;
  using Superclass::MakeOutput;

  /** Pass the input through unmodified. Do this by Grafting in the
   *  AllocateOutputs method.
   */
  void AllocateOutputs() ITK_OVERRIDE;
  void GenerateOutputInformation() ITK_OVERRIDE;
  void Synthetize(void) ITK_OVERRIDE;
  void Reset(void) ITK_OVERRIDE;

  itkSetMacro(NoDataValue, RealType);
  itkGetMacro(NoDataValue, RealType);

protected:
  PersitentMultitemporalStatisticsImageFilter();
  virtual ~PersitentMultitemporalStatisticsImageFilter() {};

  virtual void ThreadedGenerateData(const ImageRegionType& outputRegionForThread,
      itk::ThreadIdType threadId);

  /** Class for storing thread results:
   * -sum of values
   * -sum of squared values
   * -count
   */
  class ThreadResultsContainer {
  public:
    /** Default constructor */
    ThreadResultsContainer(){
    }

    /* Constructor with size */
    ThreadResultsContainer(unsigned int n)
    {
      Clear(n);
    }

    /* Copy constructor */
    ThreadResultsContainer(const ThreadResultsContainer& other)
    {
      m_count = LongMatrixType(other.m_count);
      m_sum = RealMatrixType(other.m_sum);
      m_sqSum = RealMatrixType(other.m_sqSum);
    }

    /* Clear routine: Resize at the specified dimension and clear values */
    void Clear(unsigned int n)
    {
      RealType zeroValue = itk::NumericTraits<RealType>::Zero;

      m_count = LongMatrixType(n,n,0);
      m_sum = RealMatrixType(n,n,zeroValue);
      m_sqSum = RealMatrixType(n,n,zeroValue);
    }

    /* one-pixel update */
    void Update( const RealType& pixelValue, unsigned int i, unsigned j)
    {
      m_count[i][j]++;
      m_sum[i][j] += pixelValue;
      m_sqSum[i][j] += pixelValue*pixelValue;
    }

    /* Self update */
    void Update(const ThreadResultsContainer& other)
    {
      m_count += other.m_count;
      m_sum += other.m_sum;
      m_sqSum += other.m_sqSum;
    }

    RealMatrixType m_sum;
    RealMatrixType m_sqSum;
    LongMatrixType m_count;
  };

private:
  PersitentMultitemporalStatisticsImageFilter(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented

  RealType  m_NoDataValue;
  std::vector<ThreadResultsContainer> m_StatsContainers;
  ThreadResultsContainer m_FinalStatsContainer;

}; // end of class PersistentMultitemporalStatisticsImageFilter





template<class TInputImage, class TFunctor>
class ITK_EXPORT MultitemporalStatisticsImageFilter :
  public PersistentFilterStreamingDecorator<
  PersitentMultitemporalStatisticsImageFilter<TInputImage,TFunctor> >
{
public:
  /** Standard Self typedef */
  typedef MultitemporalStatisticsImageFilter Self;
  typedef PersistentFilterStreamingDecorator
  <PersitentMultitemporalStatisticsImageFilter<TInputImage,TFunctor> > Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(MultitemporalStatisticsImageFilter, PersistentFilterStreamingDecorator);

  typedef typename Superclass::FilterType    StatFilterType;
  typedef typename StatFilterType::ImagePixelType PixelType;
  typedef typename StatFilterType::RealType  RealType;
  typedef typename StatFilterType::RealMatrixType  RealMatrixType;
  typedef TInputImage                        InputImageType;

  /** Type of DataObjects used for scalar outputs */
  typedef itk::SimpleDataObjectDecorator<RealType>  RealObjectType;
  typedef itk::SimpleDataObjectDecorator<RealMatrixType>  RealMatrixObjectType;

  using Superclass::SetInput;
  void SetInput(InputImageType * input)
  {
    this->GetFilter()->SetInput(input);
  }
  const InputImageType * GetInput()
  {
    return this->GetFilter()->GetInput();
  }

  /** Return the computed Mean. */
  RealMatrixType GetMean() const
  {
    return this->GetFilter()->GetMeanOutput()->Get();
  }
  RealMatrixObjectType* GetMeanOutput()
  {
    return this->GetFilter()->GetMeanOutput();
  }
  const RealMatrixObjectType* GetMeanOutput() const
  {
    return this->GetFilter()->GetMeanOutput();
  }

  /** Return the computed Standard Deviation. */
  RealMatrixType GetSigma() const
  {
    return this->GetSigmaOutput()->Get();
  }
  RealMatrixObjectType* GetSigmaOutput()
  {
    return this->GetFilter()->GetSigmaOutput();
  }
  const RealMatrixObjectType* GetSigmaOutput() const
  {
    return this->GetFilter()->GetSigmaOutput();
  }

  otbSetObjectMemberMacro(Filter, NoDataValue, RealType);
  otbGetObjectMemberMacro(Filter, NoDataValue, RealType);

protected:
  /** Constructor */
  MultitemporalStatisticsImageFilter() {};
  /** Destructor */
  ~MultitemporalStatisticsImageFilter() ITK_OVERRIDE {}

private:
  MultitemporalStatisticsImageFilter(const Self &); //purposely not implemented
  void operator =(const Self&); //purposely not implemented
};







} // end namespace gtb

#include <otbMultitemporalStatisticsImageFilter.hxx>


#endif /* MultitemporalStatisticsImageFilter_H_ */
