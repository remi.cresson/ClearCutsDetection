set(DOCUMENTATION "Clear Cuts Detection")

otb_module(ClearCutsDetection
  DEPENDS
    OTBCommon
    OTBApplicationEngine
    OTBStreaming
    OTBExtendedFilename
    OTBImageIO
    OTBIndices
    OTBStatistics
    OTBIOXML
    OTBConversion
    OTBStatistics
    TimeSeriesUtils
    SimpleExtractionTools
    OTBMosaic

  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  DESCRIPTION
    "Set of tools to detect clear cuts in forests"
)
