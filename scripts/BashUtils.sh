#!/bin/bash
# =========================================================================
#   Program:   Large scale clear cuts detection framework
#
#   Copyright (c) IRSTEA. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

###########################################################################
# Logging function
#
# Arguments: text to print
# Globals  : "$LOG_FILE"
#
# The text is printed through 'echo' and logged in the "$LOG_FILE" file
###########################################################################
function logging {
  logstr="[$(date)] : $@"
  echo "$logstr"
  echo "$logstr" >> "$LOG_FILE"
}

###########################################################################
# CheckFile function
#
# Arguments: path to the file to ckeck.
#
# The function throws an error if the file does not exist on the filesystem
###########################################################################
function CheckFile {
  if [ ! -f "$1" ]; then
    logging "ERROR: The file $1 does not exists!"
    exit 1
  fi
}

###########################################################################
# CheckDirectory function
#
# Arguments: path to the directory to check.
#
# The function throws an error if the directory does not exist on the filesystem
###########################################################################
function CheckDirectory {
  if [ ! -d "$1" ]; then
    logging "ERROR: The directory $1 does not exists!"
    exit 1
  fi
}

###########################################################################
# DeleteDirectory function
#
# Arguments: path to the directory to remove
#
# The function removes the directory. If the directory does not exist, an
# error is thrown.
###########################################################################
function DeleteDirectory {
  if [ -d "$1" ]; then
    if ! rm -rf "$1"; then
      logging "ERROR: Can't delete directory $1!"
      exit 1
    fi
  fi
}

###########################################################################
# DeleteFile function
#
# Arguments: path to the file to remove
#
# The function removes the file. If the file does not exist, an error is
# thrown.
###########################################################################
function DeleteFile {
  if [ -f "$1" ]; then
    if ! rm -f "$1"; then
      logging "ERROR: Can't delete file $1!"
      exit 1
    fi
  fi
}

###########################################################################
# CreateDirectory function
#
# Arguments: path to the directory to create
#
# The function creates the directory. If the directory can not be created,
# an error is thrown.
###########################################################################
function CreateDirectory {
  if [ ! -d "$1" ]; then
    if ! mkdir "$1"; then
      logging "ERROR: Can't create directory $1!"
      exit 1
    fi
  fi
}

###########################################################################
# PadInteger2 function
#
# Arguments: integer variable
#
# The function pads an integer with '0' if the length is < 2.
#
# Example:
# PadInteger2 4  --> 04 (pad with 1 zero)
# PadInteger2 14 --> 14 (no padding)
###########################################################################
function PadInteger2 {
  printf "%02d" $1
}
