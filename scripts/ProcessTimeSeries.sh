#!/bin/bash
# =========================================================================
#   Program:   Large scale clear cuts detection framework
#
#   Copyright (c) IRSTEA. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
#
#
# This script is used to process all time series located in the TILES_DIR
# Results are produced in the OUTPUT_DIR directory

cd "$( dirname "${BASH_SOURCE[0]}" )" || exit 1

# load environment variables
source LoadEnvironmentVariables.sh
source BashUtils.sh

logging "$(basename "$0"): Processing time series"

# Get current year or get year from arg if exist
if [ "$1" != "" ]; then
  logging "Processing year $1"
  CURRENT_YEAR=$1
else
  logging "Processing current year"
  CURRENT_YEAR=$(date +%Y)
fi

# loop on tiles
for TILE in "${TILES[@]}";
do
  logging "Processing tile $TILE"

  # Current tile directory
  CURRENT_TILE_DIR=${DATA_DIR}/${TILE}
  CheckDirectory "$CURRENT_TILE_DIR"

  # Get the time series filenames list
  CURRENT_TILE_LIST_FILENAME=${CURRENT_TILE_DIR}/${NDVI_COMPLETE_FILE}
  if [ ! -f "$CURRENT_TILE_LIST_FILENAME" ]; then
    logging "WARNING: $CURRENT_TILE_LIST_FILENAME does not exist"
    continue
  fi
  
  #exclude dates greater then CURRENT_YEAR
  IMAGES_FILENAMES_LIST=$(awk -F_ -v cur=$CURRENT_YEAR 'substr($2,1,4) <= cur' $CURRENT_TILE_LIST_FILENAME)
  logging "Image list:"
  logging "${IMAGES_FILENAMES_LIST[@]}"

  # Generates the dates ASCII file
  # works on SENTINEL2 products
  IMAGES_DATES_LIST=$(echo "$IMAGES_FILENAMES_LIST" | sed 's!.*/!!' | cut -f2 -d_ | cut -f1 -d-)
  logging "Dates list:"
  logging "${IMAGES_DATES_LIST[@]}"

  # Sort (IMAGES_FILENAMES_LIST, IMAGES_DATES_LIST) by ascending IMAGES_DATES_LIST
  CONCAT=$(paste -d' ' <(echo "$IMAGES_DATES_LIST") <(echo "$IMAGES_FILENAMES_LIST"))
  IMAGES_DATES_LIST=$(echo "$CONCAT" | sort | cut -f1 -d' ')
  IMAGES_FILENAMES_LIST=$(echo "$CONCAT" | sort | cut -f2 -d' ')

  # Output directory
  CURRENT_TILE_OUTPUT_DIR=${CURRENT_TILE_DIR}/${OUTPUT_DIRNAME}/${CURRENT_YEAR}
  OUTPUT_DATES_FILE=${CURRENT_TILE_OUTPUT_DIR}/dates.txt

  # Check if we need to compute the time series
  if [ -d "$CURRENT_TILE_OUTPUT_DIR" ]; then
    # if the output directory for the tile exists, we check if the dates file exists too
    if [ -f "$OUTPUT_DATES_FILE" ]; then
      # if the dates exist, check if there is the same number of dates
      if [ $(cat "$OUTPUT_DATES_FILE" | wc -l) -eq $(echo "$IMAGES_DATES_LIST" | wc -l) ]; then
        # that means no need to compute this timeseries
        logging "Tile $TILE already computed. Skiping."
        continue
      fi
    fi
  else
    CreateDirectory "$CURRENT_TILE_OUTPUT_DIR"
  fi

  # Create (or erase) the date file
  logging "Updating dates"
  echo "$IMAGES_DATES_LIST" > "$OUTPUT_DATES_FILE"

  # Logging
  OUTPUT_LOG_FILE=${CURRENT_TILE_OUTPUT_DIR}/log.txt
  DeleteFile "$OUTPUT_LOG_FILE"
  touch "$OUTPUT_LOG_FILE"
  logging "Start otbapp. See $OUTPUT_LOG_FILE"

  # Names of produced files
  OUTPUT_VECTORDATA=${CURRENT_TILE_OUTPUT_DIR}/${OUTPUT_VECTORDATA_PREFIX}${TILE}${OUTPUT_VECTORDATA_EXTENSION}
  OUTPUT_VECTORDATA_FINAL=${CURRENT_TILE_OUTPUT_DIR}/${OUTPUT_VECTORDATA_PREFIX}${TILE}${OUTPUT_VECTORDATA_SUFFIX}${OUTPUT_VECTORDATA_EXTENSION}

  # Compute time series
  otbcli_ClearCutsMultitemporalDetection -il $IMAGES_FILENAMES_LIST -dates "$OUTPUT_DATES_FILE" -masksdir "$MASKS_DIR" -outvec "$OUTPUT_VECTORDATA" -inputscale 0.0001 -forward.nmin 3 -sfilter connect -ram 2048 >> "$OUTPUT_LOG_FILE" 2>&1

  # Clip vector data on the tile envelope
  CURRENT_TILE_ENVELOPE=${ENVELOPES_DIR}/${ENVELOPES_PREFIX}${TILE}${ENVELOPES_SUFFIX}
  CheckFile "$CURRENT_TILE_ENVELOPE"
  ogr2ogr -clipdst "$CURRENT_TILE_ENVELOPE" "$OUTPUT_VECTORDATA_FINAL" "$OUTPUT_VECTORDATA" -t_srs "$OUTPUT_VECTORDATA_SRS" >> "$OUTPUT_LOG_FILE" 2>$1

done # next tile
