#!/bin/bash
# =========================================================================
#   Program:   Large scale clear cuts detection framework
#
#   Copyright (c) IRSTEA. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

source BashUtils.sh

######################### Environment variables ###########################

# Paths used for storing everything
# -temporary directories (archives download, ...)
# -ndvi
# -logs
# -output
export DATA_DIR=/home/coupesrases/DATA

# Path where the Theia Script is located
# You can create this directory then
# git clone https://github.com/olivierhagolle/theia_download.git
export THEIADOWNLOAD_SCRIPT_DIR=/home/coupesrases/scripts/theia_download/

# Path containing masks.
# Masks must be raster in GeoTiff format (.tif extension)
export MASKS_DIR=/home/coupesrases/MASK

# Path containing tiles envelopes.
# This is used to clip vector data produced on each tile.
export ENVELOPES_DIR=/home/coupesrases/ENVELOPES
export ENVELOPES_PREFIX=TILE_
export ENVELOPES_SUFFIX=.shp

# Tiles to download and process
export TILES=(T30UUU T30UVV T30UVU T30TVT T30UWA T30UWV T30UWU T30TWT T30TWS T30TWP T30TWN T30UXA T30UXV T30UXU T30TXT T30TXS T30TXR T30TXQ T30TXP T30TXN T30UYA T30UYV T30UYU T30TYT T30TYS T30TYR T30TYQ T30TYP T30TYN T31UCS T31UCR T31UCQ T31UCP T31TCN T31TCM T31TCL T31TCK T31TCJ T31TCH T31UDS T31UDR T31UDQ T31UDP T31TDN T31TDM T31TDL T31TDK T31TDJ T31TDH T31TDG T31UES T31UER T31UEQ T31UEP T31TEN T31TEM T31TEL T31TEK T31TEJ T31TEH T31UFR T31UFQ T31UFP T31TFN T31TFM T31TFL T31TFK T31TFJ T31TFH T31UGR T31UGQ T31UGP T31TGN T31TGM T31TGL T31TGK T31TGJ T31TGH T32ULV T32ULU T32TLT T32TLS T32TLR T32TLQ T32TLP T32UMV T32UMU T32TMN T32TMM T32TNN T32TNM T32TNL)

export MAX_CLOUD_COVERAGE=100
export SENSOR=SENTINEL2
export VEGETATION_PERIOD_START_MONTH=05
export VEGETATION_PERIOD_START_DAY=01
export VEGETATION_PERIOD_END_MONTH=08
export VEGETATION_PERIOD_END_DAY=31

# Suffixes for produced data (NDVI tiles, Output Vector Data)
export NDVI_DIRNAME=NDVI
export NDVI_SUFFIX=_NDVI
export NDVI_COMPLETE_FILE=ndvi_complete.txt
export OUTPUT_DIRNAME=CLEARCUTS
export OUTPUT_VECTORDATA_PREFIX=vector_data_
export OUTPUT_VECTORDATA_SUFFIX=_clip_epsg2154
export OUTPUT_VECTORDATA_EXTENSION=.shp
export OUTPUT_VECTORDATA_SRS=epsg:2154

# OTB environment
export OTB_INSTALL_DIR=/home/coupesrases/OTB/superbuild_install
export LD_LIBRARY_PATH=${OTB_INSTALL_DIR}/lib/:${OTB_INSTALL_DIR}/lib/otb/applications
export PATH=$PATH:${OTB_INSTALL_DIR}/bin

###########################################################################

# Below are basic check of environment variables

# check paths
CheckDirectory $THEIADOWNLOAD_SCRIPT_DIR
CheckDirectory $MASKS_DIR
CheckDirectory $DATA_DIR
CheckDirectory $ENVELOPES_DIR

# Logfile
export LOG_FILE=${DATA_DIR}/log.txt
touch $LOG_FILE
CheckFile $LOG_FILE
