#!/bin/bash
# =========================================================================
#   Program:   Large scale clear cuts detection framework
#
#   Copyright (c) IRSTEA. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
#
#
# This script is used to populate the database.
# It should be used to initialize it rather than update it (a lot of requests
# are transmited to THEIA in order to have a valid token during the entire
# downloading session)

cd "$( dirname "${BASH_SOURCE[0]}" )"
MAIN_SCRIPTS_DIR=$(pwd)

# load environment variables
source LoadEnvironmentVariables.sh
source BashUtils.sh

logging "$(basename "$0"): Downloading THEIA products"

# start year: 2016, end year: this year
YEAR_BEG=2016
YEAR_END=$(date +'%Y')

# Theia SSO tokens validity duration are less than 2 hours top.
# So we have to download the products sequentially rather than
# everything at the same time.
# Here we use a token to download every products of the same month
# and we loop over TILE/YEAR/MONTH to retrieve all products which are
# in the temporal range defined in the environment variables.

# number of days in months
NB_DAYS_IN_MONTH=(31 28 31 30 31 30 31 31 30 31 30 31)

# loop on tiles
for TILE in "${TILES[@]}";
  do

  logging "-------------"
  logging "Tile $TILE"
  logging "-------------"

  TILE_DIR=${DATA_DIR}/${TILE}
  CreateDirectory $TILE_DIR
  DOWNLOAD_DIR=${TILE_DIR}/download
  CreateDirectory $DOWNLOAD_DIR
  
  # clean download dir (remove temp files)
  find $DOWNLOAD_DIR -type f -name "*.tmp" -exec rm -f {} \;

  # Retrieve the list of already downloaded and processed products
  COMPLETE_FILE=${TILE_DIR}/${NDVI_COMPLETE_FILE}
  if [ -f $COMPLETE_FILE ];
  then
    EXISTING_PLIST=$(cat $COMPLETE_FILE | sed 's!.*/!!' | sed "s/${NDVI_SUFFIX}.tif//")
  fi
  logging "Already processed products:"
  logging "${EXISTING_PLIST[@]}"

  # Download the products year by year.
  # loop on years
  for YEAR in $(seq $YEAR_BEG $YEAR_END);
  do

    logging "Year: $YEAR"

    # Get the list of available products
    cd "$THEIADOWNLOAD_SCRIPT_DIR"
    PLIST=$(python theia_download.py -t $TILE -c $SENSOR -a config_theia.cfg -d $YEAR-$VEGETATION_PERIOD_START_MONTH-$VEGETATION_PERIOD_START_DAY -f $YEAR-$VEGETATION_PERIOD_END_MONTH-$VEGETATION_PERIOD_END_DAY -m $MAX_CLOUD_COVERAGE -w ${DOWNLOAD_DIR} -n | grep ^${SENSOR} | cut -f1 -d' ')

    logging "Remote products:"
    logging "${PLIST[@]}"
    TODO_LIST=$(echo "$PLIST" | grep -Fxv "$EXISTING_PLIST")

    if [ -z "$TODO_LIST" ];
    then
      logging "All products already downloaded and processed for this year."
      continue
    else
      logging "Products to download:"
      logging "${TODO_LIST[@]}"
    fi

    # loop on the TODO_LIST
    for PRODUCT in ${TODO_LIST[@]};
    do

      logging "Downloading product: $PRODUCT"

      # year of the product
      YYYY_BEG=$(echo $PRODUCT | cut -f2 -d_ | cut -f1 -d- | cut -c1-4)
      YYYY_END=$YYYY_BEG

      # begin/end months
      MM_BEG=$(echo $PRODUCT | cut -f2 -d_ | cut -f1 -d- | cut -c5-6)
      MM_BEG=$(echo $MM_BEG | sed 's/^0*//')
      MM_END=$MM_BEG

      # begin/end days
      DD_BEG=$(echo $PRODUCT | cut -f2 -d_ | cut -f1 -d- | cut -c7-8)
      DD_BEG=$(echo $DD_BEG | sed 's/^0*//')
      DD_END=$DD_BEG

      # move the end date
      let "DD_END = DD_END + 1"
      if [ "$DD_END" -gt "${NB_DAYS_IN_MONTH[$MM_END - 1]}" ];
      then
        DD_END=1
        let "MM_END = MM_END + 1"
        if [ "$MM_END" -gt "12" ];
        then
          MM_END=1
          let "YYYY_END = YEAR + 1"
        fi
      fi

      # generate start and end date
      DD_MM_YYY_BEG=${YYYY_BEG}-$(PadInteger2 ${MM_BEG})-$(PadInteger2 ${DD_BEG})
      DD_MM_YYY_END=${YYYY_END}-$(PadInteger2 ${MM_END})-$(PadInteger2 ${DD_END})
  
      python theia_download.py -t $TILE -c $SENSOR -a config_theia.cfg -d $DD_MM_YYY_BEG -f $DD_MM_YYY_END -m $MAX_CLOUD_COVERAGE -w ${DOWNLOAD_DIR} >> $LOG_FILE 2>&1

    done # next PRODUCT
  done # next YEAR

  # process .zip files
  cd $MAIN_SCRIPTS_DIR
  find $DOWNLOAD_DIR -type f -iname "*.zip" -exec ./ProcessArchive.sh {} \;

  # clean
  DeleteDirectory $DOWNLOAD_DIR
done # next tile
