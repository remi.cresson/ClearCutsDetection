#!/bin/bash
# =========================================================================
#   Program:   Large scale clear cuts detection framework
#
#   Copyright (c) IRSTEA. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
#
#
# Count images density

cd "$( dirname "${BASH_SOURCE[0]}" )"
MAIN_SCRIPTS_DIR=$(pwd)

# load environment variables
source LoadEnvironmentVariables.sh
source BashUtils.sh

logging "$(basename "$0"): Compute time series statistics"

# loop on tiles
for TILE in "${TILES[@]}";
do
  logging "Processing tile $TILE"

  # Current tile directory
  CURRENT_TILE_DIR=${DATA_DIR}/${TILE}
  CheckDirectory $CURRENT_TILE_DIR

  # Get the time series filenames list
  CURRENT_TILE_LIST_FILENAME=${CURRENT_TILE_DIR}/${NDVI_COMPLETE_FILE}
  if [ ! -f $CURRENT_TILE_LIST_FILENAME ]; then
    logging "WARNING: $CURRENT_TILE_LIST_FILENAME does not exist"
    continue
  fi

  IMAGES_FILENAMES_LIST=$(cat $CURRENT_TILE_LIST_FILENAME | sort)
  logging "Image list:"
  logging "${IMAGES_FILENAMES_LIST[@]}"

  otbcli_TimeSeriesStats -il $IMAGES_FILENAMES_LIST -nodata -20000 -out /tmp/density_$TILE.tif

done # next tile
