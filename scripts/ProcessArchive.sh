#!/bin/bash
# =========================================================================
#   Program:   Large scale clear cuts detection framework
#
#   Copyright (c) IRSTEA. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
# 
# Usage:
# $1: Archive to process
#
if [ "$#" -ne 1 ]; then
  echo "Compute NDVI from a S2 Theia archive and put it in the $TILES_DIR"
  echo "Usage: <archive.zip>"
  exit 1
fi

source LoadEnvironmentVariables.sh
source BashUtils.sh

# check parameters
CheckFile "$1"

# Get TILE/YYYY/MM/DD
YYYYMMDD=$(echo "$(basename ${1%.*})"|cut -d_ -f2|cut -d- -f1)
TILE_NAME=$(echo "$(basename ${1%.*})"|cut -d_ -f4)
YYYY=${YYYYMMDD:0:4}

TILE_OUTPUT_DIR=${DATA_DIR}/${TILE_NAME}

# Create directories arborescence
# 1 is tile dir (already existing)
# 2 is ndvi
NDVI_OUTPUT_DIR=${TILE_OUTPUT_DIR}/${NDVI_DIRNAME}
CreateDirectory "$NDVI_OUTPUT_DIR"
# 3 is Year
YYYY_OUTPUT_DIR=${NDVI_OUTPUT_DIR}/${YYYY}
CreateDirectory "${YYYY_OUTPUT_DIR}"

# Output image filename
OUTPUT_IMAGE_BASE=${YYYY_OUTPUT_DIR}/$(basename "${1%.*}")_NDVI
OUTPUT_IMAGE=$OUTPUT_IMAGE_BASE.tif
OUTPUT_LOCK=$OUTPUT_IMAGE_BASE.lock

if [ -f "$OUTPUT_IMAGE" ] && [ -f "$OUTPUT_LOCK" ]; then
  logging "There is a lock at image $OUTPUT_IMAGE ==> the image is corrupted (delete)"
  rm "$OUTPUT_IMAGE"
  rm "$OUTPUT_LOCK"
fi

if [ ! -f "$OUTPUT_IMAGE" ] && [ ! -f "$OUTPUT_LOCK" ]; then

  # unzip files
  logging "unzip $1"
  TEMP_DIR=${YYYY_OUTPUT_DIR}/temp/
  DeleteDirectory "$TEMP_DIR"
  CreateDirectory "$TEMP_DIR"
  if ! unzip "$1" '*FRE_B8.tif' '*FRE_B4.tif' '*CLM_R1.tif' '*SAT_R1.tif' -d "$TEMP_DIR"; then
    logging "ERROR: can't unzip $1 !"
    logging "Set $1 as corrupted (delete)"
    DeleteFile "$1"
    exit 1
  fi

  # compute NDVI
  # Sorted files are in the following order:
  # 1: MASK/...CLM_R1
  # 2: MASK/..SAT_R1
  # 3: B4
  # 4: B8
  touch "$OUTPUT_LOCK"
  INPUT_LIST=$(find "$TEMP_DIR" -type f -iname "*.tif" | sort)
  EXPRESSION="(im1b1==0) && (im3b1>0) && (im4b1>0) && ( ((int) im2b1 & 128) == 0) && ( ((int) im2b1 & 8) == 0) ? 10000*(im4b1-im3b1)/(im4b1+im3b1) : -20000"
  logging "compute NDVI (il=${INPUT_LIST};out=${OUTPUT_IMAGE};exp=${EXPRESSION})"
  if ! otbcli_BandMathX -il $INPUT_LIST -out "$OUTPUT_IMAGE?&gdal:co:COMPRESS=DEFLATE" int16 -exp "$EXPRESSION"; then
    logging "ERROR: can't compute NDVI from $1. il=$INPUT_LIST out=$OUTPUT_IMAGE exp=$EXPRESSION"
    exit 1
  fi
  rm "$OUTPUT_LOCK"

  DeleteDirectory "$TEMP_DIR"

  if [ -f "$OUTPUT_IMAGE" ]; then
    echo "$OUTPUT_IMAGE" >> "${TILE_OUTPUT_DIR}/$NDVI_COMPLETE_FILE"
    DeleteFile "$1"
  fi

fi
