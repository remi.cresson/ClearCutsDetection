# Clear cuts detection applications

This module contains two applications for NDVI time series processing.

## Dependencies

* The [OTB](https://www.orfeo-toolbox.org/) library
* The [TimeSeriesUtils](https://gitlab.irstea.fr/remi.cresson/TimeSeriesUtils) remote module for OTB
* The [SimpleExtractionTools](https://gitlab.irstea.fr/remi.cresson/SimpleExtractionTools) remote module for OTB

## How to build it

The module can be built like any other [otb remote module](https://wiki.orfeo-toolbox.org/index.php/How_to_write_a_remote_module). You can build it either from within OTB's sources or outside it.


## Clear cuts detection application

### Description

This application implements the clear cuts detection pipeline, developed by IRSTEA (Kenji Ose, Michel Deshayes, Rémi Cresson)

### Method

From a pair of images, (each one aquired at a different date), and a set of optional mask (input images masks, forest mask), dNDVI (the difference of the normalized difference vegetation index) is computed. Then, a threshold based on mean and standard deviation is performed to produce a map of clear cuts. The output is a vector data layer.

### How to use it

ClearCutsDetection in an OTBApplication.
It can be used as any OTB application (gui, command line, python, c++, ...).

```
This is the ClearCutsDetection (ClearCutsDetection) application, version 6.2.0

This application performs cuts detection, from two input images and an optional forest mask
Parameters: 
        -progress <boolean>        Report progress 
        -help     <string list>    Display long help (empty list), or help for given parameters keys
        -inbmask  <string>         Input vector data for T0 Image mask (Before)  (optional, off by default)
        -inamask  <string>         Input vector data for T1 Image mask (After)  (optional, off by default)
MISSING -inb      <string>         Input T0 Image (Before)  (mandatory)
MISSING -ina      <string>         Input T1 Image (After)  (mandatory)
        -masksdir <string>         Vegetation masks directory  (optional, off by default)
        -nirb     <int32>          near infrared band index for input T0 image  (mandatory, default value is 4)
        -redb     <int32>          red band index for input T0 image  (mandatory, default value is 1)
        -nira     <int32>          near infrared band index for input T1 image  (mandatory, default value is 4)
        -reda     <int32>          red band index for input T1 image  (mandatory, default value is 1)
        -filt     <int32>          Minimum number of pixels detected  (mandatory, default value is 10)
MISSING -outvec   <string>         Output vector layer  (mandatory)
        -ram      <int32>          Available RAM (Mb)  (optional, off by default, default value is 128)
        -inxml    <string>         Load otb application from xml file  (optional, off by default)

```

## Multitemporal clear cuts detection application

### Description

This application implements the clear cuts detection pipeline, developed by Rémi Cresson at IRSTEA.

### Method

Clear cuts detection are computed from a stack or multiple images (typicaly LS or S2).

### How to use it

ClearCutsMultitemporalDetection is an OTBApplication.
It can be used as any OTB application (gui, command line, python, c++, ...).

```
This is the ClearCutsMultitemporalDetection application, version 6.2.0
Performs clear cuts detection from NDVI time series

Parameters: 
        -progress              <boolean>        Report progress 
MISSING -il                    <string list>    Input time series  (mandatory)
MISSING -dates                 <string>         Input time series dates ASCII file (Must be YYYYMMDD format)  (mandatory)
        -masksdir              <string>         Vegetation masks directory  (optional, off by default)
        -backward.tmax         <int32>          Limit, in number of days, to look backward  (optional, on by default, default value is 730)
        -backward.nmin         <int32>          Minimum number of valid pixel inside backward range  (optional, on by default, default value is 5)
        -forward.tmax          <int32>          Limit, in number of days, to look forward  (optional, on by default, default value is 62)
        -forward.nmin          <int32>          Minimum number of valid pixel inside forward range  (optional, on by default, default value is 2)
        -clouds.threshold      <float>          Clouds drop threshold  (optional, on by default, default value is 0.4)
        -clouds.tmax           <int32>          Limit, in number of days, for rejecting NDVI drop due to clouds.  (optional, on by default, default value is 40)
        -veg.threshold         <float>          Vegetation threshold  (optional, on by default, default value is 0.4)
        -range.start.day       <int32>          Detection range start (day)  (optional, on by default, default value is 1)
        -range.start.month     <int32>          Detection range start (month)  (optional, on by default, default value is 5)
        -range.end.day         <int32>          Detection range end (day)  (optional, on by default, default value is 31)
        -range.end.month       <int32>          Detection range end (month)  (optional, on by default, default value is 8)
        -sfilter               <string>         Spatial filtering method [none/connect/morpho] (mandatory, default value is none)
        -sfilter.connect.size  <int32>          Minimum number of pixels  (mandatory, default value is 5)
        -sfilter.morpho.radius <int32>          Radius of the spatial filter  (optional, on by default, default value is 1)
        -sfilter.morpho.struct <string>         Structuring element type [ball/cross] (mandatory, default value is ball)
        -sfilter.morpho.op     <string>         Operation type [open] (mandatory, default value is open)
        -thresholds            <string list>    NDVI drop thresholds (defaults: 0.0 0.3 0.4 1.0)  (optional, off by default)
        -nodata                <float>          Input NDVI no-data value  (optional, on by default, default value is -2)
        -inputscale            <float>          Input scale  (optional, on by default, default value is 1)
        -outvec                <string>         Output vector layer  (optional, off by default)
        -outlabel              <string> [pixel] Output label image  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is uint8) (optional, off by default)
        -ram                   <int32>          Available RAM (Mb)  (optional, off by default, default value is 128)
        -inxml                 <string>         Load otb application from xml file  (optional, off by default)

Examples: 
otbcli_ClearCutsMultitemporalDetection -il im1.tif im2.tif ... imN.tif -dates dates.txt -outlabel cuts.shp -thresholds 0.0 0.3 0.4 0.5 1.0

```

### Additional scripts

In the _scripts_ folder, there is some bash scripts which enables the large scale processing of the multitemporal clear cuts detection application.
Here is some description of the files:
* _LoadEnvironmentVariables.sh_ contains every environment variables (paths, parameters) to deploy the framework: its the only script you need to modify,
* _DownloadTheiaProducts.sh_ once executed, this script goes to THEIA and retrieve tiles. NDVI images are then computed and stored then the entire downloaded archive is remove to save disk space,
* _ProcessTimeSeries.sh_ once executed, this script process the detection of clear cuts over the available NDVI tiles,
* _BashUtils.sh_ is a set of low level bash functions,
* _ProcessArchive.sh_ is called by _DownloadTheiaProducts.sh_ to process one downloaded archive.
Basically, the only script you need to modify is _LoadEnvironmentVariables.sh_ to set up the system. The only thing you need to do then, is to execute _DownloadTheiaProducts.sh_ then _ProcessTimeSeries.sh_.

Licence
=======

This code is provided under the CeCILL-B free software license agreement.

Contact
=======

For any issues regarding this module please contact Rémi Cresson.

remi.cresson at irstea.fr

Irstea ((French) National Research Institute of Science and Technology for Environment and Agriculture)
www.irstea.fr
